import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

const getToken = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log(`${fcmToken} fcmToken`);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    } else {
      console.log(`${fcmToken} token from storage`);
    }
  };
  
  const requestPermission = async () =>
    messaging()
      .requestPermission()
      .then(() => {
        getToken();
      })
      .catch(error => {
        console.warn(`${error} permission rejected`);
      });
  
  
  export const checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    console.log(`${enabled} hasPermission`);
    if (enabled) {
      getToken();
    } else {
      requestPermission();
    }
  };