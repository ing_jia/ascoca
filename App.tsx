import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import DrawerNavigator from './src/navigation/drawerNavigator';
import {useEffect} from 'react';
import {Alert} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import {checkPermission} from './PushNotifications';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

const App = () => {
  // const navigation = useNavigation();

  useEffect(() => {
    checkPermission().then(() => {
      const unsubscribe = messaging().onMessage(async remoteMessage => {
        console.log('FCM Message:', remoteMessage);

        // Update a users messages list using AsyncStorage
        const currentMessages = await AsyncStorage.getItem('messages');
        if (currentMessages) {
          const messageArray = JSON.parse(currentMessages);
          messageArray.push(remoteMessage.data);
          await AsyncStorage.setItem('messages', JSON.stringify(messageArray));
        }
        Alert.alert(
          'A new FCM message arrived!',
          JSON.stringify(remoteMessage),
        );
      });

      return unsubscribe;
    });
    // return unsubscribe;
  }, []);

  // TODO: Splash screen config
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <NavigationContainer>
      <DrawerNavigator />
    </NavigationContainer>
  );
};

export default App;
