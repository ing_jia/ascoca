import {IMercadoPosts} from '../../models/mercado/mercado-posts-interface';
import {IReplyPost} from '../../models/mercado/reply-post-interface';

export class MercadoService {
  basePathWs: string = 'https://www.redtrastiendaanpec.com';

  constructor() {}

  public async getInfoMercadoSection(
    wstoken: string,
    courseids: string,
  ): Promise<IMercadoPosts> {
    const options = {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: 'Basic aW5nZW5pYTpkZXNhcnJvbGxv',
      }),
    };

    if (wstoken === null) {
      throw new Error('Parametros incompletos');
    }

    const moodlewsrestformat = 'json';
    const wsfunction = 'mod_forum_get_forums_by_courses';

    const response: Response = await fetch(
      `${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&courseids[0]=${courseids}`,
      options,
    );

    if (!response.ok) {
      console.error('Respuesta de red OK pero respuesta HTTP no OK');
      throw new Error('Ocurrio un error inesperado');
    }

    return response.clone().json();
  }

  public async getPostsMercadoSection(
    wstoken: string,
    discussionid: string,
  ): Promise<IMercadoPosts> {
    const options = {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: 'Basic aW5nZW5pYTpkZXNhcnJvbGxv',
      }),
    };

    if (wstoken === null) {
      throw new Error('Parametros incompletos');
    }

    const moodlewsrestformat = 'json';
    const wsfunction = 'mod_forum_get_discussion_posts';

    const response: Response = await fetch(
      `${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&discussionid=${discussionid}`,
      options,
    );

    if (!response.ok) {
      console.error('Respuesta de red OK pero respuesta HTTP no OK');
      throw new Error('Ocurrio un error inesperado');
    }

    return response.clone().json();
  }

  public async replyPrincipalPostMercadoSection(
    wstoken: string,
    postid: string,
    subject: string,
    message: string,
  ): Promise<IReplyPost> {
    const options = {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: 'Basic aW5nZW5pYTpkZXNhcnJvbGxv',
      })
    };

    if (wstoken === null) {
      throw new Error('Parametros incompletos');
    }

    const moodlewsrestformat = 'json';
    const wsfunction = 'mod_forum_add_discussion_post';
    const messageformat = '2';

    const response: Response = await fetch(
      `${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&postid=${postid}&subject=${subject}&message=${message}&messageformat=${messageformat}`,
      options,
    );

    if (!response.ok) {
      console.error('Respuesta de red OK pero respuesta HTTP no OK');
      throw new Error('Ocurrio un error inesperado');
    }

    return response.clone().json();
  }

  public async replyPostsMercadoSection(
    wstoken: string,
    postid: string,
    subject: string,
    message: string,
  ): Promise<IReplyPost> {
    const options = {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: 'Basic aW5nZW5pYTpkZXNhcnJvbGxv',
      }),
    };

    if (wstoken === null) {
      throw new Error('Parametros incompletos');
    }

    const moodlewsrestformat = 'json';
    const wsfunction = 'mod_forum_add_discussion_post';
    const messageformat = '2';

    const response: Response = await fetch(
      `${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&postid=${postid}&subject=${subject}&message=${message}&messageformat=${messageformat}`,
      options,
    );

    if (!response.ok) {
      console.error('Respuesta de red OK pero respuesta HTTP no OK');
      throw new Error('Ocurrio un error inesperado');
    }

    return response.clone().json();
  }
}
