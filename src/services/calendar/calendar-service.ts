import { IHomeEvents } from "../../models/calendar/home-events-interface";

export class CalendarService {

    basePathWs: string = 'https://repo.ingenia.com'

    constructor() {}

    public async getCalendarUpcomingView(wstoken: string): Promise<IHomeEvents> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null) {
          throw new Error('Parametros incompletos');
        }
        const moodlewsrestformat = 'json';
        const wsfunction = 'core_calendar_get_calendar_upcoming_view';
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}