import { ILogin } from "../../models/login/login-interface";

export class LoginService {

    basePathWs: string = 'https://www.redtrastiendaanpec.com'

    constructor() {}

    public async postLogin(username: string, password:string): Promise<ILogin> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (username === null || password === null) {
          throw new Error('Credenciales incompletas');
        }

        const service = 'moodle_mobile_app';
    
        const response: Response = await fetch(`${this.basePathWs}/login/token.php?service=${service}&username=${username}&password=${password}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          // throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}