import { ICredencial } from "../../models/credencial/credencial-interface";

export class CredencialService {

    basePathWs: string = 'https://www.redtrastiendaanpec.com'

    constructor() {}

    public async getInfoCredencial(wstoken: string, key:string, value: string): Promise<ICredencial> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || key === null || value === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_user_get_users';
    
        const response: Response = await fetch(`${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&criteria[0][key]=${key}&criteria[0][value]=${value}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }

}