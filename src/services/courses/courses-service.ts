import { ICourse } from "../../models/courses/course-interface";

export class CoursesService {

    basePathWs: string = 'https://www.redtrastiendaanpec.com'

    constructor() {}

    public async getCourses(wstoken: string, courseid: string): Promise<ICourse[]> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || courseid === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_course_get_contents';
    
        const response: Response = await fetch(`${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&courseid=${courseid}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }

      public async getVideo(wstoken: string, lessonid: string): Promise<any> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || lessonid === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'mod_lesson_get_lesson';
    
        const response: Response = await fetch(`${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&lessonid=${lessonid}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}