import React from 'react';
import {View, StyleSheet, Text, ScrollView, TouchableOpacity, Linking} from 'react-native';

const Privacy = () => {
  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>Privacidad</Text>
      </View>
      <View style={styles.txtContainer}>
        <Text style={styles.txtHeaders}>
          Aviso de Privacidad Red Trastienda
        </Text>
        <Text style={styles.txtListElement}>
          • Responsable del Tratamiento de sus Datos Personales.
        </Text>
        <Text style={styles.txtListElement}>
          • Alianza Nacional de Pequeños Comerciantes, A.C. (en lo sucesivo,
          “ANPEC”), con domicilio ubicado en Rio Volga 87, Col. Cuauhtémoc,
          Delegación Cuauhtémoc, C.P. 06500, CDMX, será el responsable de
          recabar sus datos personales, del tratamiento que se les dé a los
          mismos, así como de su protección. Por tal motivo, ANPEC pone a su
          disposición el presente aviso de privacidad (en adelante, el “Aviso de
          Privacidad”), en cumplimiento con lo dispuesto en los artículos 15, 16
          y demás correlativos de la Ley Federal de Protección de Datos
          Personales en Posesión de los Particulares y su respectivo Reglamento
          (en lo sucesivo, la “Ley”). Los datos personales que usted (en
          adelante, el “Titular”) proporcione a ANPEC, de manera directa o por
          conducto de tercero debidamente autorizado para ello y a través de
          medios físicos o electrónicos, serán recabados y tratados por ANPEC,
          de conformidad con los principios de licitud, consentimiento,
          información, calidad, finalidad, lealtad, proporcionalidad y
          responsabilidad, en términos de lo dispuesto en la Ley.
        </Text>
        <TouchableOpacity
          style={styles.conForgot as any}
          onPress={() => {
            Linking.openURL('https://redtrastiendaanpec.com/aviso.html');
          }}>
          <Text style={styles.txtListElement}>• Más información en:</Text>
          <Text style={styles.txtListElementRed}>
            www.redtrastienda.com/aviso
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
  },
  txtContainer: {
    paddingHorizontal: 25,
    paddingTop: 20,
  },
  txtHeaders: {
    fontSize: 24,
    paddingTop: 20,
    fontWeight: 'bold',
    color: '#C10202',
  },
  txtListElement: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
    textAlign: 'justify',
  },
  txtListElementRed: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
    textAlign: 'justify',
    color: 'red',
  },
  txtMision: {
    paddingLeft: 20,
    fontSize: 18,
  },
  conForgot: {
    textAlign: 'left',
  },
});
export default Privacy;
