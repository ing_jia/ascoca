import React from 'react';
import {View, StyleSheet, Text, ScrollView, TouchableOpacity, Linking} from 'react-native';

const Version = () => {
  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>Contacto</Text>
      </View>
      <View style={styles.txtContainer}>
        <Text style={styles.txtListElement}>
          • Correo: c.rivera@anpec.com.mx
        </Text>
        <Text style={styles.txtListElement}>• Teléfono: 5555315989</Text>
        <Text style={styles.txtListElement}>• Versión 1.4</Text>
        <Text style={styles.txtListElement}>
          • ©2021 La Alianza Nacional De Pequeños Comerciantes A.C.
        </Text>
        <TouchableOpacity
          style={styles.conForgot as any}
          onPress={() => {
            Linking.openURL(
              'https://www.anpec.com.mx/quienes-somos/directorio/',
            );
          }}>
          <Text style={styles.txtListElement}>• Más información en:</Text>
          <Text style={styles.txtListElementRed}>
            www.anpec.com/quienes-somos/directorio
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
  },
  txtContainer: {
    paddingHorizontal: 25,
    paddingTop: 20,
  },
  txtHeaders: {
    fontSize: 24,
    paddingTop: 20,
    fontWeight: 'bold',
    color: '#C10202',
  },
  txtListElement: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
  },
  txtListElementRed: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
    textAlign: 'justify',
    color: 'red',
  },
  txtMision: {
    paddingLeft: 20,
    fontSize: 18,
  },
  conForgot: {
    textAlign: 'left',
  },
});

export default Version;
