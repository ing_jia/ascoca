import React from 'react';
import {View, StyleSheet, Text, ScrollView, Image} from 'react-native';

const Red = () => {
  return (
    <ScrollView >
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                Red Trastienda
            </Text>
        </View>
        <View>
          <Image
          source={require('../../assets/images/trastiendaInfo.png')}
          style={{width:360, height:350}}
          />
        </View>
        <View style={styles.txtContainer}>
          <Text style={styles.txtHeaders}>
            Objetivos
          </Text>
          <Text style={styles.txtListElement}>
                    EL Programa “Red Trastienda” por ANPEC, busca establecer
            una red de comunicación digital a lo largo y ancho de
            la república de pequeños comerciantes interesados en
            hacer escuchar su voz. A través de encuestas, monitoreos
            de precios, testimonios, entrevistas y otros medios de
            expresión y consulta, lograremos empujar una alianza de
            ganar-ganar junto con el gobierno y la industria. 
          </Text>
          <Text style={styles.txtHeaders}> Compromiso ANPEC </Text>
          <Text style={styles.txtMision}>
          Nuestra misión es lograr una comunidad del pequeño
comercio que sea proactiva a beneficio de sus intereses.
Todo bajo los principios éticos que den calidad a los bienes
y servicios ofrecidos. {"\n\n"}
Tales como: {"\n"}
{"      "}·Atención al cliente amable {"\n"}
{"      "}· Surtido de calidad {"\n"}
{"      "}· Promociones {"\n"}
{"      "}· Cubrir la demanda {"\n"}
{"      "}· Transparencia {"\n"}
{"      "}· Innovación {"\n"}
{"      "}· Sanidad e higiene {"\n"}
          </Text>
        </View>
    </ScrollView >
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  txtContainer:{
    paddingHorizontal:25,
  },
  txtHeaders:{
    fontSize: 24,
    paddingTop:20,
    fontWeight: 'bold',
    color: "#C10202"
  },
  txtListElement:{
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
  },
  txtMision:{
    paddingLeft: 20,
    fontSize: 18,
  }
});

export default Red;
