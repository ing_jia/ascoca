import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  Linking,
  TouchableOpacity,
} from 'react-native';

const Info = () => {
  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>ANPEC</Text>
      </View>
      <View>
        <Image
          source={require('../../assets/images/ANPECInfo.png')}
          style={{height: 300}}
        />
      </View>
      <View style={styles.txtContainer}>
        <Text style={styles.txtHeaders}>¿Quiénes somos?</Text>
        <Text style={styles.txtListElement}>
          • Somos la Alianza que agrupa a más de 75,000 pequeños comercios en
          todo el país.
        </Text>
        <Text style={styles.txtListElement}>
          • Nacimos en 2010 con el propósito de darle una identidad al comercio
          minorista
        </Text>
        <Text style={styles.txtListElement}>
          • Somos un eslabón importante dentro de la cadena de valor para el
          consumidor final.
        </Text>
        <Text style={styles.txtListElement}>
          • Somos el punto más cercano de compra al hogar, ligado a las
          necesidades básicas de cada familia.
        </Text>
        <Text style={styles.txtHeaders}> Nuestra misión </Text>
        <Text style={styles.txtMision}>
          Desde la ANPEC, nuestra misión es Brindar Bienestar al Pequeño
          Comerciante, generando las condiciones de competitividad,
          modernización y sustentabilidad necesarias para su desarrollo en el
          mercado. Como ANPEC somos la Voz Social de los Pequeños Comerciantes
          los diferentes actores sociales.
        </Text>
        <TouchableOpacity
          style={styles.conForgot as any}
          onPress={() => {
            Linking.openURL('https://www.redtrastiendaanpec.com/quienes-somos.html');
          }}>
          <Text style={styles.txtListElement}>• Más información en:</Text>
          <Text style={styles.txtListElementRed}>
            www.redtrastienda.com/quienes-somos
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
  },
  txtContainer: {
    paddingHorizontal: 25,
  },
  txtHeaders: {
    fontSize: 24,
    paddingTop: 20,
    fontWeight: 'bold',
    color: '#C10202',
  },
  txtListElement: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
  },
  txtListElementRed: {
    paddingLeft: 20,
    paddingVertical: 5,
    fontSize: 18,
    textAlign: 'justify',
    color: 'red',
  },
  txtMision: {
    paddingLeft: 20,
    fontSize: 18,
  },
  conForgot: {
    textAlign: 'left',
  },
});

export default Info;
