import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/Foundation';
import IconA from 'react-native-vector-icons/AntDesign';

const Social = () => {
  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>Redes Sociales</Text>
      </View>
      <View style={styles.body}>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            Linking.openURL(
              'https://youtube.com/channel/UCezKN_bhWzfqd7kNq1mLcKg',
            );
          }}>
          <IconA name="youtube" size={30} color="#C10202" />
          <Text style={styles.btnText}>AnpecTV</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            Linking.openURL(
              'https://instagram.com/anpecmx?utm_medium=copy_link',
            );
          }}>
          <IconA name="instagram" size={30} color="#C10202" />
          <Text style={styles.btnText}>AnpecMX</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            Linking.openURL(
              'https://twitter.com/ANPECmx?t=fRiEVRLmbkjGd57_ttHDiA&s=09',
            );
          }}>
          <Icon name="social-twitter" size={30} color="#C10202" />
          <Text style={styles.btnText}>ANPECmx</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            Linking.openURL('https://www.facebook.com/anpecmx/');
          }}>
          <Icon name="social-facebook" size={30} color="#C10202" />
          <Text style={styles.btnText}>anpecmx</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5F6FA',
    marginHorizontal: 5,
    marginRight: 10,
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
  },
  body: {
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
  },
  names: {
    fontSize: 28,
    color: '#696969',
    fontWeight: '300',
  },
  buttonContainer: {
    marginTop: 10,
    height: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 220,
    borderRadius: 35,
    backgroundColor: '#ffff',
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginLeft: 5,
  },
  btnText: {
    fontSize: 20,
    color: '#000000',
    marginLeft: 20,
    marginTop: 2,
    marginBottom: 10,
    fontFamily: 'Nunito-Bold',
  },
  carousel: {
    height: 120,
  },
});

export default Social;
