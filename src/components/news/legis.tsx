import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, FlatList, ScrollView, TouchableOpacity, Linking} from 'react-native';
import Carousel from '../../constants/carouselN';
import { LegisladoresService } from '../../services/legisladores/legisladores-service';
import {dummyNews} from '../../utils/data';
import {
  Container,
  Card,
  UserInfo,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  PostTime,
  MessageText,
  TextSection,
} from '../styles/newsStyles';

const Legis = () => {
  let legisladoresService = new LegisladoresService();
  const [upcomingNews, setUpcomingNews] = useState(dummyNews);

  legisladoresService
  .postGetNewsMisLegisladores('b6ebe494b06e55cb91a11cc3e06c586f', 'tag', 'Mis%20legisladores')
  .then(data => {
    setUpcomingNews(data.entries.map(e => {
      return {
        name: e.subject,
        //description: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
        description: e.attachmentfiles![0].fileurl,
        summary: e.summary,
        location:
          '',
        id: e.id,
      };
    }));
    // console.log(data.events);
  })
  .catch(e => {
    console.error('No se pudieron cargar los eventos', e);
    // Alert.alert('No se pudieron cargar los eventos');
  });

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Mis legisladores
        </Text>
      </View>
      <View style={styles.container}>
        <View>
          <Text style={styles.txtLastNews}>
            Últimas Noticias
          </Text>
          <View style={styles.containerNews}>
          <Carousel data={upcomingNews.slice(0,3)} />
          </View>
        </View>
      </View>
      <View> 
      <View style={styles.containerList}>
      <FlatList
        data={upcomingNews.slice(3)}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={({item}) => (
          <Card>
            <TextSection>
              <TouchableOpacity onPress={()=>{ Linking.openURL(item.summary)}}>
              <View style={styles.containerListNews}>
                <View style={styles.containerTextNews}>
                  <MessageText>{item.name}</MessageText>
                </View>
                <View>
                  <Image
                  style={styles.preview}
                  source= {{uri: item.description}}
                  />
                </View>
              </View>
              </TouchableOpacity>       
              </TextSection>
          </Card>
        )}
      />
      </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    backgroundColor: '#fff',
    padding: 10,
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  container: {
    padding: 30,
  },
  containerList: {
    alignItems: 'center',
  },
  containerTextNews: {
    width: 210,
  },
  containerListNews: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  txtLastNews: {
    textAlign: 'left',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 24,
  },
  preview: {
    width: 50,
    height: 50,
  },
  containerNews: {
    height: 240,
  },
});

export default Legis;
