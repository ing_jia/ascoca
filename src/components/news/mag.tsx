import React, { useState } from 'react';
import {View, StyleSheet, Text, Image, FlatList, ScrollView, TouchableOpacity, Linking} from 'react-native';
import { RevistaService } from '../../services/revista/revista-service';
import {
  Container,
  Card,
  Load,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  MagNumberText,
  MessageText,
  TextSection,
} from '../styles/magazineStyles';

const MagazinesDummy = [
  {
    id:'1',
    magNumber: 'Revista #39',
    titleMagazine: 'Desafios de la Macroeconomía',
    uri: '../../assets/images/magazine.png',
    pdf: ''
  },
];

const Mag = () => {

  let revistaService = new RevistaService();
  const [magazines, setMagazines] = useState(MagazinesDummy);

  revistaService
  .postGetNewsRevistaTrastienda('b6ebe494b06e55cb91a11cc3e06c586f', 'tag', 'Revista%20trastienda')
  .then(data => {
    setMagazines(data.entries.map(e => {
      return {
        id: e.id.toString(),
        magNumber: e.subject,
        titleMagazine: e.summary,
        uri: e.attachmentfiles![0].fileurl,
        pdf: e.attachmentfiles[1] ? e.attachmentfiles[1].fileurl : 'https://google.com',
      };
    }));
    // console.log(data.events);
  })
  .catch(e => {
    console.error('No se pudieron cargar los eventos', e);
    // Alert.alert('No se pudieron cargar los eventos');
  });

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Revista Trastienda
        </Text>
      </View>
      <View style={styles.containerFilter}>
      <Text>
          Filtrar Revistas
        </Text>
      </View>

      <View  style={styles.containerList}>
      <FlatList 
        data={magazines}
        keyExtractor={item=>item.id}
        renderItem={({item}) => (
          <Card style={styles.containerCard}>
            <TouchableOpacity onPress={()=>{Linking.openURL(item.pdf)}}>
            <View style={styles.card}>
            <View>
                  <Image
                  style={styles.preview}
                  source= {{uri: item.uri}}
                  />
            </View>
            <TextSection>
              <View style={styles.containerInfo}>
                <View style={styles.containerTitle}>
                  <MagNumberText>{item.magNumber}</MagNumberText>
                  <MessageText>{item.titleMagazine}</MessageText>
                </View>
                <View style={styles.imageView}>
                  <Image
                  style={styles.view}
                  source={require('../../assets/images/QR.png')}
                  />
                </View>
              </View>
            </TextSection>
              </View>
              </TouchableOpacity>
          </Card>
        )}
      />
      <Load style={styles.loadMore}>
      <Text style={styles.loadText}>
          Cargar más revistas
        </Text>
      </Load>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    padding: 10,
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  containerFilter:{
    backgroundColor: '#fff',
    padding: 20,
    margin: 20,
    marginBottom: -15,
  },
  containerList: {
    alignItems: 'center',
    padding: 20,
    margin: 20,
  },
  containerCard: {
    height: 600,
  },
  card: {
    backgroundColor: '#fff',
  },
  containerTitle: {
    width: 250,
   
  },
  containerInfo: {
    width: 300,
    display: 'flex',
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'space-around'
    
  },
  preview: {
    width: 311,
    height: 440,
  },
  imageView: {
    width: 20,
  },
  view: {
    width:20,
    height: 16,
  },
  loadMore: {
    backgroundColor: '#fff',
    padding: 20,
    margin: 30,
    marginBottom: 15,
    borderColor:'red',
    borderWidth:1,
    borderRadius:5,
    alignItems:'center',
        
  },
  loadText: {
    color: 'red',
        
  },
});

export default Mag;