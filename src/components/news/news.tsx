import React, { useState } from 'react';
import {View, StyleSheet, Text, Image, FlatList, ScrollView, TouchableOpacity, Linking} from 'react-native';
import { RevistaService } from '../../services/revista/revista-service';
import {
  Container,
  Card,
  Load,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  MagNumberText,
  MessageText,
  TextSection,
} from '../styles/boletinStyles';

const MagazinesDummy = [
  {
    id:'1',
    magNumber: 'Boletín #35',
    titleMagazine: 'Inflación',
    uri: '../../assets/images/anpecLogo.png'
  },
  {
    id:'2',
    magNumber: 'Revista #35',
    titleMagazine: 'Para activar la economía, los apoyos anunciados',
    uri: '../../assets/images/anpecLogo.png'
  },
  {
    id:'3',
    magNumber: 'Revista #34',
    titleMagazine: 'Inflación',  
    uri: '../../assets/images/anpecLogo.png'
  },
  {
    id:'4',
    magNumber: 'Revista #33',
    titleMagazine: 'Se pulveriza el aumento de salario enero-2020',  
    uri: '../../assets/images/anpecLogo.png'
  },
    {
    id:'5',
    magNumber: 'Revista #32',
    titleMagazine: 'TMEC un salto cuántico-diciembre 2019',
    uri: '../../assets/images/anpecLogo.png'
  },
  {
    id:'6',
    magNumber: 'Revista #31',
    titleMagazine: 'Salud y Gobernación garantizar libre tránsito...',
    uri: '../../assets/images/anpecLogo.png'
  },
];

const News = () => {

  let revistaService = new RevistaService();
  const [news, setNews] = useState(MagazinesDummy);

  revistaService
  .postGetNewsRevistaTrastienda('b6ebe494b06e55cb91a11cc3e06c586f', 'tag', 'Noticias ANPEC')
  .then(data => {
    setNews(data.entries.map(e => {
      return {
        id: e.id.toString(),
        magNumber: e.subject,
        titleMagazine: e.summary,
        uri: e.attachmentfiles![0].fileurl
      };
    }));
    // console.log(data.events);
  })
  .catch(e => {
    console.error('No se pudieron cargar los eventos', e);
    // Alert.alert('No se pudieron cargar los eventos');
  });  

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Noticias ANPEC
        </Text>
      </View>
      <View>
      <Text style={styles.containerFilter}>
          Últimos Boletines
        </Text>
      </View>

      <View  style={styles.containerList}>
      <FlatList 
        data={news}
        keyExtractor={item=>item.id}
        renderItem={({item}) => (
          <Card style={styles.containerCard}>
            <TouchableOpacity>
            <View style={styles.card}>
            <View style={styles.containerLogo}>
                  <Image
                  style={styles.preview}
                  source= {{uri: item.uri}}
                  />
            </View>
            <TextSection>
              <View style={styles.containerInfo}>
                <View style={styles.containerTitle}>
                  <MagNumberText>{item.magNumber}</MagNumberText>
                  <MessageText>{item.titleMagazine}</MessageText>
                </View>
                <View style={styles.imageView}>
                  <Image
                  style={styles.view}
                  source={require('../../assets/images/downloadButton.png')}
                  />
                </View>
              </View>
           </TextSection>
                </View>
              </TouchableOpacity>
            </Card>
          )}
        />
        <Load style={styles.loadMore}>
          <Text style={styles.loadText}>Cargar más boletines</Text>
        </Load>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    padding: 10,
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  containerFilter: {
    paddingLeft: 30,
    paddingTop: 30,
    textAlign: 'left',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 24,
  },
  containerList: {
    alignItems: 'center',
    padding: 20,
    margin: 20,
  },
  containerCard: {
    height: 300,
  },
  card: {
    backgroundColor: '#fff',
    height: 273,
  },
  containerTitle: {
    width: 300,
  },
  containerInfo: {
    width: 300,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  containerLogo: {
    width: 320,
    display: 'flex',
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 60,
  },
  preview: {
    width: 188,
    height: 46.81,
  },
  imageView: {
    width: 20,
  },
  view: {
    width: 20,
    height: 16,
  },
  loadMore: {
    backgroundColor: '#fff',
    padding: 20,
    margin: 30,
    marginBottom: 15,
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
  },
  loadText: {
    color: 'red',
  },
});

export default News;
