import React, {useState, useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  Linking,
} from 'react-native';

import {ILogin} from '../../models/login/login-interface';
import {LoginService} from '../../services/login/login-service';
import {CredencialService} from '../../services/credencial/credencial-service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firestore from '@react-native-firebase/firestore';

const Login = (props: any) => {
  const [name, setName] = useState(''); // coordinador1
  const [password, setPassword] = useState(''); // Ingenia12$
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [userStorage, setUserStorage] = useState({ id: '', email: '', token: '' });

  useEffect(() => {
    getUser();
  }, []);

  async function getUser() {
    let userLocal = await AsyncStorage.getItem('user');
    if (userLocal) {
      setUserStorage(JSON.parse(userLocal));
      console.log('userStorage', JSON.parse(userLocal));
      props.navigation.navigate('Inicio', {});
    } 
  }

  let loginService = new LoginService();
  let credencialService = new CredencialService();

  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/logob.png')}
        style={styles.brand}
      />
      <Text style={styles.texta}>Ingresar</Text>
      <View style={styles.inputView}>
        <TextInput
          value={name}
          style={styles.inputText}
          placeholder="Usuario"
          placeholderTextColor="#130b07"
          onChangeText={text => setName(text)}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          value={password}
          secureTextEntry
          style={styles.inputText}
          placeholder="Contraseña"
          placeholderTextColor="#130b07"
          onChangeText={text => setPassword(text)}
        />
      </View>

      <TouchableOpacity 
        style={styles.loginBtn}
        onPress={async () => {
          try {
            let responseLogin: ILogin = await loginService.postLogin(
              name,
              password,
            );
            if (responseLogin.token == null) {
              Alert.alert(responseLogin.error);
            } else {

              credencialService
              .getInfoCredencial('b6ebe494b06e55cb91a11cc3e06c586f', 'username', name)
              .then(data => {
                //console.log('data from credencial', data)
                let userLocal = {id: name, email: data.users![0].email, token: responseLogin.token};
                AsyncStorage.setItem('user', JSON.stringify(userLocal)).then(async storage => {
                  console.log('Usuario guardado ', userLocal);

                  let fcmToken = await AsyncStorage.getItem('fcmToken');

                  firestore()
                  .collection('USERS')
                  // Filter results
                  .where('email', '==', userLocal.email)
                  .get()
                  .then(querySnapshotU => {
                    console.log('Total users: ', querySnapshotU.size);

                    querySnapshotU.forEach(async documentSnapshotU => {
                      console.log('User ID: ', documentSnapshotU.id, documentSnapshotU.data());

                      let userPermission = documentSnapshotU.data();

                      await firestore()
                      .collection('USERS')
                      .doc(documentSnapshotU.id)
                      .set(
                        {
                          token: fcmToken
                        },
                        { merge: true },
                      );

                    });
                  });
                });
              })
              .catch(e => {
                console.error(e);
                //Alert.alert('No se pudieron cargar las conversaciones');
              });

              props.navigation.navigate('Inicio', {token: responseLogin});
            }
          } catch (e) {
            console.error(e);
          }
        }}>
        <Text style={styles.txtButton}> Ingresar </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.conForgot as any}
        onPress={() => {
          Linking.openURL(
            'https://www.redtrastiendaanpec.com/login/forgot_password.php',
          );
        }}>
        <Text style={styles.forgot}>¿Has olvidado tu contraseña?</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F6FA',
    justifyContent: 'space-evenly',
    paddingHorizontal: 24,
    paddingBottom: 77,
    paddingTop: 10,
  },
  texta: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#130b07',
    marginBottom: 20,
    fontFamily: 'Nunito-Bold',
  },
  brand: {
    height: 60,
    resizeMode: 'contain',
    marginVertical: 50,
    alignSelf: 'center',
  },
  inputView: {
    backgroundColor: '#ffff',
    borderColor: '#6F7074',
    borderWidth: 1,
    borderRadius: 5,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 50,
    color: '#000000',
  },
  forgot: {
    color: '#C10202',
    fontSize: 13,
  },
  conForgot: {
    textAlign: 'left',
  },
  loginBtn: {
    backgroundColor: '#C10202',
    borderRadius: 5,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10,
    fontFamily: 'Nunito',
  },
  loginText: {
    color: '#ffff',
  },
  txtButton: {
    color: '#fff',
    fontSize: 22,
    fontFamily: 'Nunito-Bold',
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Login;
