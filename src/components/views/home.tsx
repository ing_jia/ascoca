import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Carousel from '../../constants/carousel';
import {CalendarService} from '../../services/calendar/calendar-service';
import {dummyEvents} from '../../utils/data';
import messaging from '@react-native-firebase/messaging';

const Home = (props: any) => {
  let calendarService = new CalendarService();
  const [upcomingEvents, setUpcomingEvents] = useState(dummyEvents);
  useEffect(() => {
    // Assume a message-notification contains a "type" property in the data payload of the screen to open
    // onNotificationOpenedApp When the application is running, but in the background.
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage,
      );
      // navigation.navigate(remoteMessage.data.type);
      props.navigation.navigate('News Screen', {});
    });

    // Check whether an initial notification is available
    // getInitialNotification When the application is opened from a quit state.
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage,
          );
          //  setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
      });
  }, []);

  calendarService
    .getCalendarUpcomingView('b6ebe494b06e55cb91a11cc3e06c586f')
    .then(data => {
      setUpcomingEvents(data.events);
      // console.log(data.events);
    })
    .catch(e => {
      console.error('No se pudieron cargar los eventos', e);
      // Alert.alert('No se pudieron cargar los eventos');
    });

  return (
    <View style={styles.container}>
      <View style={styles.carousel}>
        <Carousel data={upcomingEvents} />
      </View>
      <View style={styles.body}>
        <View>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('Groups Screen', {})}>
            <Icon name="message-square" size={30} color="#C10202" />
            <Text style={styles.btnText}>Chats</Text>
            <Icon
              name="chevron-right"
              size={20}
              color="#C10202"
              style={{paddingLeft: 185}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('News Screen', {})}>
            <Icon name="calendar" size={30} color="#C10202" />
            <Text style={styles.btnText}>
              Enlaces de Interés {'\n'}del Pequeño Comercio
            </Text>
            <Icon
              name="chevron-right"
              size={20}
              color="#C10202"
              style={{paddingLeft: 35}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('Train Screen', {})}>
            <Icon name="award" size={30} color="#C10202" />
            <Text style={styles.btnText}>Capacitación</Text>
            <Icon
              name="chevron-right"
              size={20}
              color="#C10202"
              style={{paddingLeft: 113}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('Market Screen', {})}>
            <IconM name="message-text-outline" size={30} color="#C10202" />
            <Text style={styles.btnText}>Mercado</Text>
            <Icon
              name="chevron-right"
              size={20}
              color="#C10202"
              style={{paddingLeft: 150}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('About Screen', {})}>
            <Icon name="credit-card" size={30} color="#C10202" />
            <Text style={styles.btnText}>Quiénes somos</Text>
            <Icon
              name="chevron-right"
              size={20}
              color="#C10202"
              style={{paddingLeft: 90}}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5F6FA',
    marginHorizontal: 5,
    marginRight: 10,
  },
  body: {
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
  },
  names: {
    fontSize: 28,
    color: '#696969',
    fontWeight: '300',
  },
  buttonContainer: {
    marginTop: 10,
    height: 80,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10,
    width: 320,
    borderRadius: 7,
    backgroundColor: '#ffff',
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginLeft: 5,
  },
  btnText: {
    fontSize: 20,
    color: '#000000',
    marginLeft: 10,
    marginTop: 2,
    marginBottom: 10,
    fontFamily: 'Nunito-Bold',
  },
  carousel: {
    height: 120,
  },
});

export default Home;
