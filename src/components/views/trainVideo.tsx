import React, { useState } from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import Video from 'react-native-video';
import { CoursesService } from '../../services/courses/courses-service';

const TrainVideo = (props: any) => {

  console.log(props)

  let coursesService = new CoursesService();
  const [uri, setUri] = useState('');

  coursesService
  .getVideo('b6ebe494b06e55cb91a11cc3e06c586f', props.route.params.module.instance)
  .then(data => {
    //setCourses(data);
    setUri(data.lesson ? data.lesson.intro: 'https://prod-015845-mexicococacola-cloudfronts3content-12ei93trh58xd.s3.us-west-1.amazonaws.com/5by20-bucket/videos/Introduccion/I.1.+Bienvenida.mp4');
    console.log(data);
  })
  .catch(e => {
    console.error('No se pudo cargar el video', e);
  });  
  
  return (
    <ScrollView >
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                Capacitación
            </Text>
        </View>
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                
            </Text>
        </View>
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                
            </Text>
        </View>
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                
            </Text>
        </View>
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                
            </Text>
        </View>
        <View style={styles.containerHeader}>
            <Text style={styles.txtHeader}>
                
            </Text>
        </View>
        
        <Video 
            source={{uri}}   // Can be a URL or a local file.
            controls={true}
            paused={true}
            resizeMode="contain"
            style={styles.backgroundVideo} />

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader2: {
    textAlign: 'left',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 24,
    paddingHorizontal: 20
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 40,
    left: 0,
    bottom: 60,
    right: 0,
    margin: 10,
  },
});

export default TrainVideo;
