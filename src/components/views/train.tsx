import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  ScrollView,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import {CoursesService} from '../../services/courses/courses-service';
import {dummyCourses} from '../../utils/data';
import Icon from 'react-native-vector-icons/Feather';
import {
  Container,
  Card,
  Load,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  MagNumberText,
  MessageText,
  TextSection,
} from '../styles/boletinStyles';

const Train = (props: any) => {
  let coursesService = new CoursesService();
  const [courses, setCourses] = useState(dummyCourses);

  coursesService
    .getCourses('b6ebe494b06e55cb91a11cc3e06c586f', '2')
    .then(data => {
      //setCourses(data);
      console.log(data);
    })
    .catch(e => {
      console.error('No se pudieron cargar los eventos', e);
      // Alert.alert('No se pudieron cargar los eventos');
    });

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>Capacitación</Text>
      </View>
      <View>
        <Text style={styles.containerFilter}>
          Manejo de inventario y cobranza para pequeños comerciantes.
        </Text>
      </View>

      <View>
        <Text style={styles.titleList}>Contenido del curso</Text>
      </View>
      <View style={styles.containerList}>
        <FlatList
          data={courses}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => (
            <View>
              <Card style={styles.containerCard}>
                <View>
                  <View style={styles.card}>
                    <TextSection>
                      <View style={styles.containerInfo}>
                        <View style={styles.containerTitle}>
                          <MessageText>{item.name}</MessageText>
                        </View>
                        <View style={styles.imageView}>
                          <Image
                            style={styles.view}
                            source={require('../../assets/images/downloadButton.png')}
                          />
                        </View>
                      </View>
                    </TextSection>
                  </View>
                </View>
              </Card>
              <FlatList
                data={item.modules}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('Video Screen', {module: item})
                    }>
                    <Text style={styles.itemListElement}>
                      <Icon name="play-circle" size={30} color="#C10202"></Icon>{' '}
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                )}></FlatList>
            </View>
          )}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    padding: 10,
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
  },
  containerFilter: {
    paddingLeft: 30,
    paddingTop: 30,
    textAlign: 'left',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 26,
  },
  titleList: {
    paddingLeft: 30,
    paddingTop: 30,
    textAlign: 'left',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 26,
  },
  containerList: {
    alignItems: 'center',
    padding: 20,
    margin: 20,
  },
  containerCard: {
    height: 75,
    width: 310,
  },
  card: {
    backgroundColor: '#fff',
    height: 60,
    width: 310,
  },
  containerTitle: {
    width: 250,
  },
  containerInfo: {
    width: 300,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  containerLogo: {
    width: 320,
    display: 'flex',
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 60,
  },
  preview: {
    width: 188,
    height: 46.81,
  },
  imageView: {
    width: 20,
  },
  view: {
    width: 20,
    height: 16,
  },
  loadMore: {
    backgroundColor: '#fff',
    padding: 20,
    margin: 30,
    marginBottom: 15,
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
  },
  loadText: {
    color: 'red',
  },
  itemListElement: {
    display: 'flex',
    paddingBottom: 20,
    paddingLeft: 20,
    color: '#C10202',
    fontSize: 18,
    justifyContent: 'flex-end',
  },
});

export default Train;
