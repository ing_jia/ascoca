import React, { useState, useContext, useEffect } from 'react';
import {
  GiftedChat,
  Bubble,
  Send,
  SystemMessage,
  Composer,
} from 'react-native-gifted-chat';
import {
  ActivityIndicator, View, Text, Platform,
  PermissionsAndroid,
  Dimensions,
  Alert,
  KeyboardAvoidingView,
  StyleSheet
} from 'react-native';
import { AudioRecorder, AudioUtils } from "react-native-audio";
import Sound from "react-native-sound";
import Ionicons from "react-native-vector-icons/Ionicons";
import { IconButton } from 'react-native-paper';
import { AuthContext } from '../navigation/AuthProvider';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import messaging from '@react-native-firebase/messaging';
import useStatsBar from '../utils/useStatusBar';
import NavigationBar from "react-native-navbar";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';


function ChatScreen({ route, navigation }) {
  // audio and images
  const [currentTime, setCurrentTime] = useState(0.0);
  const [startAudio, setStartAudio] = useState(false);
  const [hasPermission, setHasPermission] = useState(undefined);

  const [playAudio, setPlayAudio] = useState(false);
  // -- audio and images
  const [userStorage, setUserStorage] = useState({ id: '', email: '', token: '' });
  const [messages, setMessages] = useState([]);
  const { thread } = route.params;

  async function handleSend(messages) {

    await firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .add({
        text: messages[0].text,
        messageType: "message",
        image: "",
        audio: "",
        createdAt: new Date().getTime(),
        user: {
          _id: userStorage.id,
          email: userStorage.email,
        },
      });

    await firestore()
      .collection('THREADS')
      .doc(thread._id)
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime(),
          },
        },
        { merge: true },
      );
  }

  useEffect(() => {
    getUser();
    const messagesListener = firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .orderBy('createdAt', 'desc')
      .onSnapshot(querySnapshot => {
        const messages = querySnapshot.docs.map(doc => {
          const firebaseData = doc.data();

          const data = {
            _id: doc.id,
            text: firebaseData.messageType === "message" ? firebaseData.text : "",
            image: firebaseData.messageType === "image" ? firebaseData.image : "",
            audio: firebaseData.messageType === "audio" ? firebaseData.audio : "",
            messageType: firebaseData.messageType,
            createdAt: new Date().getTime(),
            ...firebaseData,
          };

          if (!firebaseData.system) {
            data.user = {
              ...firebaseData.user,
              name: firebaseData.user.email,
            };
          }

          return data;
        });
        setMessages(messages);
      });

    checkPermission();

    // Stop listening for updates whenever the component unmounts
    return () => messagesListener();
  }, [thread._id]);

  const checkPermission = () => {
    if (Platform.OS !== "android") {
      return Promise.resolve(true);
    }
    const rationale = {
      title: "Microphone Permission",
      message:
        "AudioExample needs access to your microphone so you can record audio."
    };
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      rationale
    ).then(result => {
      console.log("Permission result:", result);
      return result === true || result === PermissionsAndroid.RESULTS.GRANTED;
    }).then( async isAuthorised => {

      setHasPermission(isAuthorised);

      if (!isAuthorised) return;

    });
  };

  async function getUser() {
    let userLocal = await AsyncStorage.getItem('user');
    if (userLocal) setUserStorage(JSON.parse(userLocal));
  }

  const renderName = (props) => {
    // const { user: self } = props; // where your user data is stored;
    const user = props.currentMessage.user;
    /* const pUser= props.previousMessage.user;
    const isSameUser = pUser._id === user._id; */
    const shouldNotRenderName = false;
    return shouldNotRenderName ? (
      <View />
    ) : (
      <View>
        <Text style={{ color: "grey", padding: 2, alignSelf: "center" }}>
          {`${user.name}.`}
        </Text>
      </View>
    );
  };

  // funcion de renderizado del microfono en el chat
  const renderAudio = (props) => {
    return !props.currentMessage.audio ? (
      <View />
    ) : (
      <Ionicons
        name="ios-play"
        size={35}
        color={playAudio ? "blue" : "blue"}
        style={{
          left: 90,
          position: "relative",
          shadowColor: "#000",
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 0.5,
          backgroundColor: "transparent"
        }}
        onPress={() => {
          setPlayAudio(true);
          const sound = new Sound(props.currentMessage.audio, "", error => {
            if (error) {
              console.log("failed to load the sound", error);
            }
            setPlayAudio(false);
            sound.play(success => {
              console.log(success, "success play");
              if (!success) {
                Alert.alert("There was an error playing this audio");
              }
            });
          });
        }}
      />
    );
  };

  function renderBubble(props) {
    return (
      <View>
        {/*renderName(props)*/}
        {renderAudio(props)}
        <Bubble
          {...props}
          wrapperStyle={{
            right: {
              backgroundColor: '#C10202',
            },
          }}
          textStyle={{
            right: {
              color: '#fff',
            },
          }}
        />
      </View>
    );
  }

  const handleAudio = async () => {

    if (!hasPermission) {
      console.warn('Can\'t record, no permission granted!');
      return;
    }

    try {
      if (!startAudio) {

        await AudioRecorder.prepareRecordingAtPath(`${AudioUtils.DocumentDirectoryPath
        }/audio_${messageIdGenerator()}.aac`, {
          SampleRate: 22050,
          Channels: 1,
          AudioQuality: "Low",
          AudioEncoding: "aac",
          MeteringEnabled: true,
          IncludeBase64: true,
          AudioEncodingBitRate: 32000
        });
  
        AudioRecorder.onProgress = (data) => {
          setCurrentTime(Math.floor(data.currentTime));
          console.log(data, "onProgress data");
        };
  
        AudioRecorder.onFinished = (data) => {
          //console.log(data, 'onFinished data');
          console.log('onFinished audio');
          // Android callback comes in the form of a promise instead.
          if (Platform.OS === 'ios') {
            _finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
          }
        };

        setStartAudio(true);
        const filePath = await AudioRecorder.startRecording();
      } else {
        setStartAudio(false);

        const filePath = await AudioRecorder.stopRecording();

        if (Platform.OS === 'android') {
          _finishRecording(true, filePath);
        }

        let storageRef = storage().ref().child('audios')
          .child(`audio_${new Date().getTime()}.aac`);
        storageRef
          .putFile(filePath).then(function (snapshot) {
            console.log('Uploaded an audio!', snapshot);

            storageRef.getDownloadURL().then(url => {

              console.log('download url ', url);

              firestore()
              .collection('THREADS')
              .doc(thread._id)
              .collection('MESSAGES')
              .add({
                text:'',
                messageType: "audio",
                image: "",
                audio: url,
                createdAt: new Date().getTime(),
                user: {
                  _id: userStorage.id,
                  email: userStorage.email,
                },
              });

            }).catch(e => {
              console.error('cant get public url', e);
            });

          }).catch(error => {
            console.log('Ocurrio un error guardando el audio ', error);
          });

      }
    } catch (error) {
      console.error(error);
    }
  };

  const _finishRecording = (didSucceed, filePath, fileSize) => {
    console.log(`Finished recording of duration ${currentTime} seconds at path: ${filePath} and size of ${fileSize || 0} bytes`);
  }

  const messageIdGenerator = () => {
    // generates uuid.
      return new Date().getTime().toString();
  }

  const handleAddCamera = () => {
    const options = {
      title: "Select Profile Pic",
      mediaType: "photo",
      takePhotoButtonTitle: "Take a Photo",
      maxWidth: 256,
      maxHeight: 256,
      allowsEditing: true,
      noData: true,
      durationLimit: 15,
      includeBase64: true
    };
    launchCamera(options, response => {
    //launchImageLibrary(options, response => {
      // console.log("Response = ", response);
      if (response.didCancel) {
        // do nothing
      } else if (response.errorCode) {
        // alert error
      } else {
        const asset = response.assets[0];

        let storageRef = storage().ref().child('images')
          .child(asset.fileName);
        storageRef
          .putString(asset.base64.toString(), 'base64').then(function (snapshot) {
            console.log('Uploaded a base64 string!', snapshot);

            storageRef.getDownloadURL().then(url => {

              console.log('download url ', url);

              firestore()
                .collection('THREADS')
                .doc(thread._id)
                .collection('MESSAGES')
                .add({
                  text: "",
                  messageType: "image",
                  image: url,
                  audio: "",
                  createdAt: new Date().getTime(),
                  user: {
                    _id: userStorage.id,
                    email: userStorage.email,
                  },
                });
            }).catch(e => {
              console.error('cant get public url', e);
            });

          }).catch(error => {
            console.log('Ocurrio un error guardando el archivo ', error);
          });
      }
    });
  };

  const handleAddPicture = () => {
    const options = {
      title: "Select Profile Pic",
      mediaType: "photo",
      takePhotoButtonTitle: "Take a Photo",
      maxWidth: 256,
      maxHeight: 256,
      allowsEditing: true,
      noData: true,
      durationLimit: 15,
      includeBase64: true
    };
    //launchCamera(options, response => {
    launchImageLibrary(options, response => {
      // console.log("Response = ", response);
      if (response.didCancel) {
        // do nothing
      } else if (response.errorCode) {
        // alert error
      } else {
        const asset = response.assets[0];

        let storageRef = storage().ref().child('images')
          .child(asset.fileName);
        storageRef
          .putString(asset.base64.toString(), 'base64').then(function (snapshot) {
            console.log('Uploaded a base64 string!', snapshot);

            storageRef.getDownloadURL().then(url => {

              console.log('download url ', url);

              firestore()
                .collection('THREADS')
                .doc(thread._id)
                .collection('MESSAGES')
                .add({
                  text: "",
                  messageType: "image",
                  image: url,
                  audio: "",
                  createdAt: new Date().getTime(),
                  user: {
                    _id: userStorage.id,
                    email: userStorage.email,
                  },
                });
            }).catch(e => {
              console.error('cant get public url', e);
            });

          }).catch(error => {
            console.log('Ocurrio un error guardando el archivo ', error);
          });
      }
    });
  };

  function renderLoading() {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#C10202" />
      </View>
    );
  }

  function renderSend(props) {
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          <IconButton icon="send-circle" size={32} color="#C10202" />
        </View>
      </Send>
    );
  }

  function scrollToBottomComponent() {
    return (
      <View style={styles.bottomComponentContainer}>
        <IconButton icon="chevron-double-down" size={36} color="#C10202" />
      </View>
    );
  }

  function renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        wrapperStyle={styles.systemMessageWrapper}
        textStyle={styles.systemMessageText}
      />
    );
  }

  function renderImage(props){
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          <IconButton icon="send-circle" size={32} color="#C10202" />
        </View>
      </Send>
    );
  };

  function renderComposer(props){

    return (
      <View style={{flexDirection: 'row'}}>
        <IconButton icon="camera" size={32} color="#C10202" onPress={handleAddCamera} style={{paddingTop:-1, marginTop:-5}}/>
        <IconButton icon="attachment" size={32} color="#C10202" onPress={handleAddPicture} style={{paddingTop:-1, marginTop:-5, marginLeft:-15}}/>
        <Ionicons
                name="ios-mic"
                size={32}
                color={startAudio ? "black" : "#C10202"}
                style={{
                  shadowColor: "#000",
                  shadowOffset: { width: 0, height: 0 },
                  shadowOpacity: 0.5,
                  zIndex: 2,
                  backgroundColor: "transparent",
                  marginLeft:-10
                }}
                onPress={handleAudio}
              />

        <Composer {...props} />
        <Send {...props}>
        <View style={styles.sendingContainer}>
          <IconButton icon="send-circle" size={32} color="#C10202" />
        </View>
      </Send>
      </View>
    );
  }

  const rightButtonConfig = {
    title: 'Add photo',
    handler: () => handleAddPicture(),
  };

  return (
    <View style={{ flex: 1 }}>
      <NavigationBar
        title={{ title: thread.name}}
        
      />
      <GiftedChat
        messages={messages}
        onSend={handleSend}
        user={{ _id: userStorage.id }}
        placeholder="Mensaje..."
        alwaysShowSend
        showUserAvatar
        scrollToBottom
        renderBubble={renderBubble}
        messageIdGenerator={messageIdGenerator}
        renderComposer={renderComposer}
        renderActions={() => {
          if (Platform.OS === "ios") {
            return (
              <Ionicons
                name="ios-mic"
                size={35}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                color={startAudio ? "red" : "black"}
                style={{
                  bottom: 50,
                  right: Dimensions.get("window").width / 2,
                  position: "absolute",
                  shadowColor: "#000",
                  shadowOffset: { width: 0, height: 0 },
                  shadowOpacity: 0.5,
                  zIndex: 2,
                  backgroundColor: "transparent"
                }}
                onPress={handleAudio}
              />
            );
          }
        }}
        renderLoading={renderLoading}
        renderSend={renderSend}
        scrollToBottomComponent={scrollToBottomComponent}
        renderSystemMessage={renderSystemMessage}
        renderMessageAudio={() => {
          <View />
        }}
      />
      <KeyboardAvoidingView />
    </View>
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sendingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomComponentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  systemMessageWrapper: {
    backgroundColor: '#C10202',
    borderRadius: 4,
    padding: 5,
  },
  systemMessageText: {
    fontSize: 14,
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default ChatScreen;
