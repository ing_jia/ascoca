import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {List, Divider} from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import Loading from '../commons/Loading';
import useStatsBar from '../utils/useStatusBar';
import AsyncStorage from '@react-native-async-storage/async-storage';


function GroupsScreen({ navigation }) {
    const [user, setUser] = useState({id: '', email: '', token: ''});
    const [threads, setThreads] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getUser();/*
        const unsubscribe = firestore()
        .collection('THREADS')
        .orderBy('latestMessage.createdAt', 'desc')
        .onSnapshot(querySnapshot => {
            const threads = querySnapshot.docs.map(documentSnapshot => {

            return {
                _id: documentSnapshot.id,
                // give defaults
                name: '',

                latestMessage: {
                text: '',
                },
                ...documentSnapshot.data(),
            };
            });

            setThreads(threads);

            if (loading) {
            setLoading(false);
            }
        });*/

        /**
         * unsubscribe listener
         */
        //return () => unsubscribe();
      }, []);

      if (loading) {
        return <Loading />;
      }

    async function getUser() {
        let userLocal = JSON.parse(await AsyncStorage.getItem('user'));
        if (userLocal) setUser(userLocal);

        firestore()
        .collection('USERS')
        // Filter results
        .where('email', '==', userLocal.email)
        .get()
        .then(querySnapshotU => {
          console.log('Total users: ', querySnapshotU.size);

          querySnapshotU.forEach(documentSnapshotU => {
            console.log('User ID: ', documentSnapshotU.id, documentSnapshotU.data());

            let userPermission = documentSnapshotU.data();

            const unsubscribe = firestore()
            .collection('THREADS')
            //.orderBy('latestMessage.createdAt', 'desc')
            .onSnapshot(querySnapshotT => {

              console.log('Total threads: ', querySnapshotT.size);
                const threads = querySnapshotT.docs.filter(document => userPermission.groups.includes(document.id) ).map(documentSnapshotT => {
                
                  console.log('Thread ID: ', documentSnapshotT.id, documentSnapshotT.data());
                return {
                    _id: documentSnapshotT.id,
                    // give defaults
                    name: '',

                    latestMessage: {
                    text: '',
                    },
                    ...documentSnapshotT.data(),
                };
                });

                setThreads(threads);

                if (loading) {
                setLoading(false);
                }
            });
          });
        });
    }


    return (
        <View style={styles.container}>
          <FlatList
            data={threads}
            keyExtractor={item => item._id}
            ItemSeparatorComponent={() => <Divider />}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => navigation.navigate('Chat Screen', {thread: item})}>
                <List.Item
                  title={item.name}
                  description={item.latestMessage.text}
                  titleNumberOfLines={1}
                  titleStyle={styles.listTitle}
                  descriptionStyle={styles.listDescription}
                  descriptionNumberOfLines={1}
                />
              </TouchableOpacity>
            )}
          />
        </View>
      );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f5f5f5',
      flex: 1,
    },
    listTitle: {
      fontSize: 22,
    },
    listDescription: {
      fontSize: 16,
    },
  });
  

export default GroupsScreen