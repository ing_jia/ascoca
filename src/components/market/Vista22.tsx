import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import ProfileImage from '../commons/ProfileImage';
import {MercadoService} from '../../services/mercado/mercado-service';

const Vista22 = () => {
  const [formValues, setFormValues] = useState({message: '', image: ''});
  const mercadoService = new MercadoService();

  const postSubmit = async () => {
    if (formValues.message === '') {
      return;
    }
    console.log(formValues);
    try {
      const data = await mercadoService.replyPrincipalPostMercadoSection(
        'b6ebe494b06e55cb91a11cc3e06c586f',
        '2',
        'prueba titulo',
        formValues.message,
      );
      console.log('response Posts:', data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View style={styles.center}>
          <Text style={styles.title}>Mercado</Text>
        </View>
        <View style={{backgroundColor: '#fff'}}>
          <View style={styles.profile}>
            <ProfileImage src="https://robohash.org/user" />
            <Text style={styles.name}>Luis Resendiz</Text>
          </View>
          <View>
            <TextInput
              value={formValues.message}
              style={styles.pubic}
              placeholder="Publicar..."
              placeholderTextColor="#130b07"
              onChangeText={text =>
                setFormValues(actual => ({...actual, message: text}))
              }
            />
          </View>
        </View>
        <View style={styles.footer}>
          <Text style={styles.footerText}>Imagen</Text>
        </View>
      </ScrollView>

      <View>
        <TouchableOpacity style={styles.button} onPress={postSubmit}>
          <Text style={styles.buttonText}>Publicar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    paddingVertical: 30,
    backgroundColor: '#eee',
  },
  title: {
    color: '#000',
    fontSize: 30,
    fontWeight: 'bold',
  },
  footer: {
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: '#eee',
    height: 'auto',
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
  },
  footerText: {
    color: '#000',
    fontSize: 18,
    fontWeight: 'bold',
  },
  profile: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
  },
  name: {
    fontSize: 16,
    color: '#000',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  pubic: {
    marginLeft: 20,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'red',
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Vista22;
