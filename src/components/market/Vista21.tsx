import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Button from '../commons/Button';
import Card from '../commons/Card';
import Comment from '../commons/Comment';
import NewPost from '../commons/NewPost';

const Vista21 = () => {
  return (
    <View style={{flex: 1}}>
      <View style={styles.center}>
        <Text style={styles.title}>Mercado</Text>
      </View>
      <ScrollView>
        <View>
          <Text style={styles.post}>
            Hola companeros, pongo a su dispocision una serie de productos para
            su comercio
          </Text>
        </View>

        <View style={styles.containerImage}>
          <Image
            resizeMode="cover"
            style={[styles.img, {overflow: 'visible'}]}
            source={{
              uri: 'https://i.ibb.co/vz2K2gZ/Screenshot-2021-11-12-at-12-57-47-Flow-1.png',
            }}
          />
        </View>

        <View>
          <Button text="Todos los comentarios" />
        </View>

        <View style={styles.comments}>
          <Comment
            user="Luis Resendiz"
            comment="Este es un texto random para un comentario random"
          />
          <Comment
            user="Luis Resendiz"
            comment="Este es otro texto que es dirtinto y random para un comentario random"
          />
          <Comment
            user="Luis Resendiz"
            comment="Este es un texto mas largo y random mas para un comentario random"
          />
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <View style={styles.input}>
          <TextInput value="Responder..." />
          <TouchableOpacity>
            <Text>Enviar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  comments: {
    flex: 1,
    flexDirection: 'column-reverse',
    marginVertical: 10,
    marginHorizontal: 20,
  },
  post: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    color: '#000',
    fontSize: 16,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    paddingVertical: 30,
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
  },
  title: {
    color: '#000',
    fontSize: 30,
    fontWeight: 'bold',
  },
  footer: {
    backgroundColor: 'red',
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  input: {
    borderRadius: 100,
    height: 40,
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    fontSize: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 240,
  },
  img: {
    flex: 1,
    width: '100%',
  },
});

export default Vista21;
