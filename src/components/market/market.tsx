import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Card from '../commons/Card';
import NewPost from '../commons/NewPost';
import {MercadoService} from '../../services/mercado/mercado-service';
import {IPost} from '../../models/mercado/post-interface';

const Market = () => {
  const [posts, setPosts] = useState<Array<IPost>>([]);
  const mercadoService = new MercadoService();

  useEffect(() => {
    getPostsInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const getPostsInfo = async () => {
    try {
      const data = await mercadoService.getPostsMercadoSection(
        'b6ebe494b06e55cb91a11cc3e06c586f',
        '2',
      );
      const dataMaped: Array<IPost> = await data.posts;
      setPosts(dataMaped);
      console.log('response Posts:', data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>Mercado</Text>
      </View>
      <View style={styles.containerImage}>
        <Image
          resizeMode="cover"
          style={[styles.img, {overflow: 'visible'}]}
          source={{
            uri: 'https://i.ibb.co/vz2K2gZ/Screenshot-2021-11-12-at-12-57-47-Flow-1.png',
          }}
        />
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.title}>Mercado Red Trastienda</Text>
        <Text style={styles.text}>
          Red Trastienda es un espacio seguro para promocionar tus productos,
          servicios y conocimietos.
        </Text>
        <Text style={styles.textBold}>
          Esta red de apoyo es para que JUNTOS salgamos adelante.
        </Text>
      </View>

      <NewPost callback={getPostsInfo} />

      <View style={styles.recentContainer}>
        <Text style={styles.textBold}>Publicaciones Recientes</Text>
      </View>

      <FlatList
        data={posts}
        renderItem={({item}) => <Card key={item.id} post={item} />}
        keyExtractor={item => String(item.id)}
      />

      <View style={styles.footerContainer}>
        <Text style={styles.footerText}>ANPEC &copy; 2021</Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  containerHeader: {
    backgroundColor: '#fff',
    paddingVertical: 10,
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 34,
    color: '#000',
    fontWeight: 'bold',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 240,
  },
  img: {
    flex: 1,
    width: '100%',
  },
  containerTitle: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: '#eee',
  },
  title: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 24,
    color: '#000',
    fontWeight: 'bold',
  },
  statistics: {
    paddingVertical: 10,
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  text: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
    color: '#000',
  },
  textBold: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
  },
  recentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#eee',
  },
  footerContainer: {
    backgroundColor: 'red',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  footerText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default Market;
