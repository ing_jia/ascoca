import React, {Component, useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image,TouchableOpacity,ScrollView, PermissionsAndroid, StatusBar, SafeAreaView, Alert} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import { CredencialService } from '../../services/credencial/credencial-service';
import { dummyCredencial } from '../../utils/data';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import AsyncStorage from '@react-native-async-storage/async-storage';

const iD = (props: any) => {

  let credencialService = new CredencialService();
  const [credencial, setCredencial] = useState(dummyCredencial.users![0]);
  const [userStorage, setUserStorage] = useState({ id: '', email: '', token: '' });

  useEffect(() => {
    console.log(`props`, props)
    let focusListener = props.navigation.addListener('focus',
       () => { 
               console.log('focus is called'); 
              //your logic here.
              getUser();
       }
     );
  }, []);

  async function getUser() {
    let userLocal = await AsyncStorage.getItem('user');
    let userLocalObj:any = JSON.parse(userLocal ? userLocal : '');
    if (userLocal) setUserStorage(JSON.parse(userLocal));

    console.log('userLocal', userLocal);
    console.log('userStorage', userStorage);
    credencialService
    .getInfoCredencial('b6ebe494b06e55cb91a11cc3e06c586f', 'username', userLocalObj.id)
    .then(data => {
      //console.log('data from credencial', data)
      setCredencial(data.users![0]);
    })
    .catch(e => {
      console.error(e);
      //Alert.alert('No se pudieron cargar las conversaciones');
    });

  }


    const htmlContent = (credencialInfo: any) => {
      return `
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>
      </head>
      <body>
          <div>
      
          <h3>Credencial Trastienda</h3>
          <div style="border-width: 1px;border-style: solid; border-color: grey;">
              <div style="background-color: rgb(206, 206, 206);">
                  <p style="text-align: end;">Folio: ${credencialInfo.id}</p>
              </div>
              <div>
                  <div>
                      <div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
                          <div>
                              <img src="${credencialInfo!.profileimageurl}"/>
                          </div>
                          <div>
                              <h6>${credencialInfo.fullname}</h6>
                              <h3>${credencialInfo.customfields![0].value}</h3>
                              <p>${credencialInfo.customfields![0].value}</p>
                          </div>
                      </div>
                  </div>
                  <div style="text-align: center;">
                    <img src="${credencialInfo!.profileimageurlsmall}"/>
                  </div>
              </div>
              <div style="background-color: rgb(206, 206, 206);">
                  <p style="text-align: center;">vigencia:</p>
              </div>
          </div>
          </div>
      </body>
      </html>
      `;
    }

const requestPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "Permisos de almacenamiento",
        message:
          "Para descargar tu credencial " +
          "necesitas brindar permisos de almacenamiento",
        buttonNegative: "Cancelar",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can save the file");
      createPdf();
    } else {
      console.log("Storage permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

const makeid = (length:number) =>{
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * 
charactersLength));
 }
 return result;
}

const generatePdfName = (name:string) =>{
  var pdfName = name.substring(0, name.indexOf(" "));
  pdfName = pdfName + makeid(4);
  return pdfName;
} 

   const createPdf = async () => {
    let options = {

      html: htmlContent(credencial),
      fileName: `${generatePdfName(credencial.fullname)}`,
      directory: 'Download',
    };

    let file = await RNHTMLtoPDF.convert(options)
    // console.log(file.filePath);
    Alert.alert("Credencial generada, puede verla en: "+file.filePath);
  }

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Credencial Trastienda
        </Text>
      </View>
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image
              style={styles.avatar}
              source={{uri: credencial!.profileimageurl}}
            />
            <Text style={styles.userInfo}> Folio: {credencial.id}</Text>
            <Text style={styles.name}>{credencial.fullname} </Text>
            <Text style={styles.userStore}>"{credencial.customfields![0].value}" </Text>
            <Text style={styles.userAdrss}>
            {credencial.customfields![0].value}
            </Text>
            <Image
              style={styles.avatar}
              source={require('../../assets/images/QR.png')}
            />
          </View>
        </View>
        <View style={styles.expDate}>
          <Text style={styles.txtExpDate}>Vigencia 06/22</Text>
        </View>
        <TouchableOpacity 
        style={styles.dwnBtn}
        onPress={requestPermission }
        >
        <Text style={styles.txtButton}> Descargar Credencial </Text>
          
      </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  container: {
    padding: 30,
  },
  header: {
    backgroundColor: '#ffff',
  },
  headerContent: {
    padding: 30,
    alignItems: 'center',
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: 'white',
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    color: '#000000',
    fontWeight: '600',
    paddingVertical: 5,
  },
  userInfo: {
    fontSize: 18,
    color: '#6F7074',
    fontWeight: '600',
    paddingBottom: 5,
  },
  userStore: {
    fontSize: 28,
    color: '#000000',
    fontWeight: 'bold',
    paddingVertical: 5,
  },
  userAdrss: {
    textAlign: 'center',
    fontSize: 18,
    color: '#6F7074',
    paddingVertical: 12,
  },
  expDate: {
    backgroundColor: '#E1E1E1',
    paddingVertical: 10,
  },
  txtExpDate: {
    textAlign: 'center',
    color: '#6F7074'
  },
  dwnBtn: {
    backgroundColor: '#fff',
    width: '100%',
    borderColor: '#C10202',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginTop: 10,
  },
  txtButton: {
    color: '#C10202',
    textAlign: 'center',
    fontSize: 18,
  },
});

export default iD;
