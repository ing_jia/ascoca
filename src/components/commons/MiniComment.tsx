import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from './Button';
import ProfileImage from './ProfileImage';

const MiniComment: React.FC<{
  user: string;
  comment: string;
}> = ({user, comment}) => {
  return (
    <View style={styles.container}>
      <ProfileImage src="https://robohash.org/usermini" />
      <View style={styles.right}>
        <View style={styles.box}>
          <Text style={styles.user}>{user}</Text>
          <Text style={styles.comment}>{comment}</Text>
        </View>
        <Button text="Responder" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 10,
  },
  right: {
    flex: 1,
    marginLeft: 10,
  },
  box: {
    backgroundColor: '#eee',
    padding: 10,
    borderRadius: 5,
  },
  user: {
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#000',
    fontSize: 16,
  },
  comment: {
    color: '#000',
  },
});

export default MiniComment;
