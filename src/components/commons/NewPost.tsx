import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import Button from './Button';
import ProfileImage from './ProfileImage';
import {MercadoService} from '../../services/mercado/mercado-service';

const NewPost = props => {
  const [formValues, setFormValues] = useState({message: '', image: ''});
  const mercadoService = new MercadoService();

  const postSubmit = async () => {
    if (formValues.message === '') {
      return;
    }
    console.log(formValues);
    try {
      const data = await mercadoService.replyPrincipalPostMercadoSection(
        'b6ebe494b06e55cb91a11cc3e06c586f',
        '2',
        'prueba titulo',
        formValues.message,
      );
      props.callback();
      setFormValues({message: '', image: ''});
      console.log('response Posts:', data);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <View>
      <View style={styles.newPostContainer}>
        <ProfileImage src="https://robohash.org/123" />
        <View>
          <TextInput
            value={formValues.message}
            style={{marginLeft: 15, minWidth: '50%', marginRight: 15}}
            placeholder="Publicar..."
            placeholderTextColor="#130b07"
            multiline
            numberOfLines={4}
            onChangeText={text =>
              setFormValues(actual => ({...actual, message: text}))
            }
          />
        </View>
      </View>
      <View style={styles.body}>
        <TouchableOpacity style={styles.button} onPress={postSubmit}>
          <Text style={styles.buttonText}>Publicar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#C10202',
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    height: 40,
    alignItems: 'center',
    marginBottom: 20,
    width: 120,
    borderRadius: 35,
    marginLeft: 5,
  },
  body: {
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  newPostContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
});

export default NewPost;
