import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
//import {Post} from '../App';
import {IPost} from '../../models/mercado/post-interface';
import Button from './Button';
import ProfileImage from './ProfileImage';

interface User {
  id: string;
  name: String;
}

const Card: React.FC<{post: IPost}> = ({post}) => {
  console.log('dataPost', post);

  const date = new Date(post.timecreated * 1000);

  return (
    <View style={styles.cardContainer}>
      {post && (
        <React.Fragment>
          <View style={styles.headerContainer}>
            <ProfileImage src={post.author.urls.profileimage} />
            <View style={styles.userInfo}>
              <Text style={styles.userName}>{post.author.fullname}</Text>
              <Text style={{color: '#6F7074'}}>
                {date.toLocaleDateString()}
              </Text>
            </View>
          </View>
          <View style={styles.postContainer}>
            <Text style={styles.postText}>{post.message}</Text>
          </View>
          {post.images?.length > 1 && (
            <View style={styles.postImages}>
              {post.images.map((uri, i) => (
                <View style={styles.containerImage}>
                  <Image
                    key={i}
                    resizeMode="cover"
                    style={[styles.img, {overflow: 'visible'}]}
                    source={{uri}}
                  />
                </View>
              ))}
            </View>
          )}
          {post.images?.length === 1 && (
            <View style={styles.postImage}>
              <View style={styles.containerOneImage}>
                <Image
                  resizeMode="cover"
                  style={[styles.img, {overflow: 'visible'}]}
                  source={{uri: post.images[0]}}
                />
              </View>
            </View>
          )}
        </React.Fragment>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: '#fff',
    flex: 1,
    paddingTop: 25,
    marginBottom: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  userInfo: {
    paddingHorizontal: 20,
    flex: 1,
  },
  userName: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  postContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  postText: {
    fontSize: 16,
    color: '#000',
  },
  postImage: {
    marginBottom: 10,
  },
  postImages: {
    marginBottom: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  commentsContainer: {
    paddingHorizontal: 20,
  },
  comments: {
    color: '#6F7074',
    marginBottom: 10,
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    height: 120,
  },
  containerOneImage: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 200,
  },
  img: {
    flex: 1,
    width: '100%',
  },
});

export default Card;
