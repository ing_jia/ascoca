import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

const ProfileImage: React.FC<{src: string}> = ({src}) => {
  return (
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        source={{
          uri: src,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    width: 40,
    height: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    borderRadius: 100,
    overflow: 'hidden',
  },
  image: {
    width: 40,
    height: 40,
  },
});

export default ProfileImage;
