import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const Button: React.FC<{ text?: string }> = ({ text = 'Button text' }) => {
	return (
		<View style={styles.view}>
			<TouchableOpacity style={styles.containerButtton}>
				<Text style={styles.textButton}>{text}</Text>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	view: {
		borderTopColor: '#eee',
		borderTopWidth: 1
	},
	containerButtton: {
		flex: 1,
		paddingVertical: 10,
		paddingHorizontal: 15
	},
	textButton: {
		color: '#000',
		fontWeight: 'bold',
		fontFamily: 'Nunito-ExtraBold',
		letterSpacing: 0.4,
		fontSize: 16
	}
});

export default Button;
