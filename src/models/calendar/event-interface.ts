export interface IEvent {
    id: number;
    name: string;
    description: string;
    descriptionformat: number;
    location: string;
    categoryid: number;
    groupid: number;
    userid: number;
    repeatid: number;
    eventcount: any;
    component: any;
    modulename: string;
    instance: any;
    eventtype: string;
    timestart: number;
    timeduration: number;
    timesort: number;
    timeusermidnight: number
    visible: number;
    timemodified: number;
    icon: {
        key: string;
        component: string;
        alttext: string
    };
    subscription: {
        displayeventsource: boolean
    };
    canedit: boolean;
    candelete: boolean;
    deleteurl: string;
    editurl: string;
    viewurl: string;
    formattedtime: string;
    isactionevent: boolean;
    iscourseevent: boolean;
    iscategoryevent: boolean;
    groupname: any;
    normalisedeventtype: string;
    normalisedeventtypetext: string;
    url: string;
    islastday: boolean;
    popupname: string;
    draggable: boolean
}