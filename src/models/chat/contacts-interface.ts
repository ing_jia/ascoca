import { IMember } from "./member-interface";

export interface IContacts {
    contacts: Array<IMember>;
    noncontacts: Array<IMember>;
}