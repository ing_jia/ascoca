import { IConversation } from "./conversation-interface";

export interface IConversations {
    conversations: IConversation[];
}