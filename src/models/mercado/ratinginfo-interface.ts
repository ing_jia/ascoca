export interface IRatinginfo {
    contextid: number;
    component: string;
    ratingarea: string;
    canviewall: any;
    canviewany: any;
    scales: any [],
    ratings: any []
}