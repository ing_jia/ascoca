import { IMessage } from "./message-interface";
import { IPost } from "./post-interface";

export interface IReplyPost {
    postid: number;
    warnings: any[];
    post: IPost;
    messages: IMessage[];
}