export interface IUrl {
    view: string;
    viewisolated: string;
    viewparent: string | null;
    edit: string;
    delete: string;
    split: string | null;
    reply: string;
    export: any;
    markasread: any;
    markasunread: any;
    discuss: string;
}