export interface IAuthor {
    id: number;
    fullname: string;
    isdeleted: boolean;
    groups: [];
    urls: {
        profile: string;
        profileimage: string;
    }
}