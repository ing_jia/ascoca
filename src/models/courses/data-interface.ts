export interface IData {
    state: number;
    timecompleted: number;
    overrideby: null;
    valueused: boolean;
    hascompletion: boolean;
    isautomatic: boolean;
    istrackeduser: boolean;
    uservisible: boolean;
    details: [];
}