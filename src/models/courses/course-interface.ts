import { IModule } from "./modules-interface";

export interface ICourse {
    modules:IModule[];
    id: number;
    name: string;
    visible: number;
    summary: string;
    summaryformat: number;
    section:number;
    hiddenbynumsections:number;
    uservisible: boolean;
    
}