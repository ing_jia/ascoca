import { IData } from "./data-interface";

export interface IModule {
    id: number;
    url: string;
    name: string;
    instance: number;
    contextid: number;
    visible: number;
    uservisible: boolean;
    availabilityinfo?: string;
    visibleoncoursepage: number;
    modicon: string;
    modname: string;
    modplural: string;
    availability: string | null;
    indent: number;
    onclick: "";
    afterlink: null;
    customdata: "\"\"";
    noviewlink: boolean;
    completion: number;
    completiondata: IData;
    dates: [];
}