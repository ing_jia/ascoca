import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Feather';
import IconM from 'react-native-vector-icons/MaterialIcons';
import IconS from 'react-native-vector-icons/Foundation';
import IconO from 'react-native-vector-icons/Octicons';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  HomeStackNavigator,
  AccountStackNavigator,
  ChatStackNavigator,
} from './StackNavigator';
import Login from '../components/views/login';
import Train from '../components/views/train';
import {NewsTabNavigator} from './tabNavigator';
import {AboutTabNavigator} from './tabNavigator';
import {View, Text} from 'react-native';
import HeaderHome from '../common/headerHome';
import HeaderView from '../common/headerView';
import Market from '../components/market/market';
import Privacy from '../components/about/privacy';
import Social from '../components/about/social';
import Version from '../components/about/version';

const Drawer = createDrawerNavigator();

const CustomDrawer = (props: any) => {
  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView {...props}>
        <View
          style={{
            padding: 20,
            paddingBottom: 0,
          }}>
          <View>
            <Text
              style={{
                fontSize: 40,
                fontFamily: 'Nunito-Bold',
              }}>
              Menú
            </Text>
          </View>
        </View>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
    </View>
  );
};

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: true,
        headerStyle: {
          elevation: 0,
          shadowOpacity: 0,
        },
        headerTitle: '',
      }}
      drawerContent={props => <CustomDrawer {...props} />}>
      <Drawer.Screen
        name="Login"
        component={Login}
        options={{
          drawerLabel: () => null,
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Inicio"
        component={HomeStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Inicio',
          drawerIcon: () => <Icon name="home" size={30} color="#C10202" />,
          headerTitle: () => <HeaderHome />,
        }}
      />
      <Drawer.Screen
        name="Account"
        component={AccountStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Mi cuenta',
          drawerIcon: () => <Icon name="user" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Chats"
        component={ChatStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Chats',
          drawerIcon: () => (
            <Icon name="message-square" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="News"
        component={NewsTabNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Noticias de Pequeño Comercio',
          drawerIcon: () => <Icon name="calendar" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Train Screen"
        component={Train}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Capacitación',
          drawerIcon: () => <Icon name="award" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Market Screen"
        component={Market}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Mercado',
          drawerIcon: () => (
            <IconMC name="message-text-outline" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Inf"
        component={AboutTabNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 20,
            color: '#000',
          },
          drawerLabel: 'Quiénes Somos',
          drawerIcon: () => <Icon name="users" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Red Screen"
        component={Social}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Redes Sociales',
          drawerIcon: () => (
            <IconS name="social-skillshare" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Priv Screen"
        component={Privacy}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Privacidad',
          drawerIcon: () => (
            <IconM name="privacy-tip" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Ver Screen"
        component={Version}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Contacto',
          drawerIcon: () => <IconO name="versions" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
