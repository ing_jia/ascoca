import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import ChatScreen from '../components/chat/ChatScreen';
import GroupsScreen from '../components/chat/GroupsScreen';

import News from '../components/news/news';
import Legis from '../components/news/legis';
import Mag from '../components/news/mag';
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';

import Bene from '../components/benefits/bene';
import Promo from '../components/benefits/promo';
import Caravan from '../components/benefits/caravan';
import AddGroupScreen from '../components/chat/AddGroupScreen';

import Info from '../components/about/info';
import Red from '../components/about/red';

const Tab = createBottomTabNavigator();

const ChatsTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Groups Screen"
        component={GroupsScreen}
        options={{title: 'Groups', headerShown: false}}
      />
      <Tab.Screen
        name="Chat Screen"
        component={ChatScreen}
        options={{title: 'Chats', headerShown: false}}
      />
      <Tab.Screen
        name="Add Group Screen"
        component={AddGroupScreen}
        options={{title: 'Add Group'}}
      />
    </Tab.Navigator>
  );
};

const NewsTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Mis Legisladores"
        component={Legis}
        options={{
          headerShown: false,
          tabBarIcon: () => <Icon name="user" size={30} color="#C10202" />,
        }}
      />
      <Tab.Screen
        name="Noticias ANPEC"
        component={News}
        options={{
          headerShown: false,
          tabBarIcon: () => <Icon name="file" size={30} color="#C10202" />,
        }}
      />
      <Tab.Screen
        name="Revista Trastienda"
        component={Mag}
        options={{
          headerShown: false,
          tabBarIcon: () => <Icon name="book-open" size={30} color="#C10202" />,
        }}
      />
    </Tab.Navigator>
  );
};

const AboutTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="ANPEC"
        component={Info}
        options={{
          headerShown: false,
          tabBarIcon: () => <Icon2 name="home" size={30} color="#C10202" />,
        }}
      />
      <Tab.Screen
        name="REDtrastienda"
        component={Red}
        options={{
          headerShown: false,
          tabBarIcon: () => <Icon2 name="store" size={30} color="#C10202" />,
        }}
      />
    </Tab.Navigator>
  );
};

const BeneTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Bene" component={Bene} options={{headerShown: false}} />
      <Tab.Screen
        name="Promo"
        component={Promo}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Caravan"
        component={Caravan}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

export {
  ChatsTabNavigator,
  NewsTabNavigator,
  BeneTabNavigator,
  AboutTabNavigator,
};
