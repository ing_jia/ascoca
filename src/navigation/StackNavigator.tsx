import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../components/views/home';
import iD from '../components/account/iD';
import GroupsScreen from '../components/chat/GroupsScreen';
import ChatScreen from '../components/chat/ChatScreen';
import AddGroupScreen from '../components/chat/AddGroupScreen';
import Train from '../components/views/train';
import TrainVideo from '../components/views/trainVideo';
import Market from '../components/market/market';
import Privacy from '../components/about/privacy';
import Social from '../components/about/social';
import Version from '../components/about/version';
import {NewsTabNavigator, AboutTabNavigator} from './tabNavigator';

const Stack = createNativeStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: '#ffff',
  },
  gestureDirection: 'horizontal-inverted',
  animationEnabled: false,
};

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Home Screen"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen name="iD" component={iD} options={{headerShown: false}} />
      <Stack.Screen
        name="Groups Screen"
        component={GroupsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Chat Screen"
        component={ChatScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Add Group Screen"
        component={AddGroupScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="News Screen"
        component={NewsTabNavigator}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Train Screen"
        component={Train}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Video Screen"
        component={TrainVideo}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="About Screen"
        component={AboutTabNavigator}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Market Screen"
        component={Market}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Red Screen"
        component={Social}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Priv Screen"
        component={Privacy}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Ver Screen"
        component={Version}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const AccountStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="iD" component={iD} options={{headerShown: false}} />
      <Stack.Screen
        name="Home Screen"
        component={Home}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const ChatStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Groups Screen"
        component={GroupsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Chat Screen"
        component={ChatScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home Screen"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Add Group Screen"
        component={AddGroupScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export {HomeStackNavigator, AccountStackNavigator, ChatStackNavigator};
