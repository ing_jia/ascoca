import { IContacts } from "../models/chat/contacts-interface";
import { IConversation } from "../models/chat/conversation-interface";
import { IConversations } from "../models/chat/conversations-interface";
import { ICourse } from "../models/courses/course-interface";
import { ICredencial } from "../models/credencial/credencial-interface";
import { IEntries } from "../models/legisladores/entries-interface";
import { IMercadoPosts } from "../models/mercado/mercado-posts-interface";
import { IReplyPost } from "../models/mercado/reply-post-interface";

export const dummyEvents = [
  {
    name: '¡Caravana! 22,23,y 24 de Septiembre CDMX',
    description: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
    location:
      'Caravana',
    id: 1,
  }
];
export const dummyNews = [
    {
      name: 'Nuevo sistema de cobro para agilizar los almacenes en Colima',
      description: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
      location:
        'Colima',
      id: 1,
      summary: ''
    },
    {
        name: 'Nuevo sistema de cobro para agilizar los almacenes en Colima',
        description: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
        location:
          'CDMX',
        id: 2,
        summary: ''
    }
  ];

export const dummyCredencial: ICredencial = {
  "users": [
    {
        "id": 5,
        "username": "alumno3",
        "firstname": "Alumno Tres",
        "lastname": "Ingenia",
        "fullname": "Alumno Tres Ingenia",
        "email": "jcuacuam@hotmail.com",
        "department": "",
        "firstaccess": 1631297962,
        "lastaccess": 1632250955,
        "auth": "manual",
        "suspended": false,
        "confirmed": true,
        "lang": "es_mx",
        "theme": "",
        "timezone": "99",
        "mailformat": 1,
        "description": "Fábrica de Software",
        "descriptionformat": 1,
        "country": "MX",
        "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f2?rev=1200",
        "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f1?rev=1200",
        "customfields": [
            {
                "type": "text",
                "value": "Abarrotes Gena",
                "name": "Nombre del negocio",
                "shortname": "business_name"
            },
            {
                "type": "text",
                "value": "Vicente Guerrero #315, Coyoacán, CDMX. Sección 35",
                "name": "Dirección del negocio",
                "shortname": "business_address"
            },
            {
                "type": "datetime",
                "value": "1664514000",
                "name": "Vigencia credencial",
                "shortname": "validity"
            }
        ]
    }
],
"warnings": []
};

export const dummyCourses: ICourse[] = [
    {
        "id": 1,
        "name": "Introducción",
        "visible": 1,
        "summary": "",
        "summaryformat": 1,
        "section": 0,
        "hiddenbynumsections": 0,
        "uservisible": true,
        "modules": [
            {
                "id": 20,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=20",
                "name": "Bienvenida",
                "instance": 1,
                "contextid": 140,
                "visible": 1,
                "uservisible": true,
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": null,
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 1,
                    "timecompleted": 1634583671,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 21,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=21",
                "name": "Ver el Mundo a Través de los Lentes Morados",
                "instance": 2,
                "contextid": 141,
                "visible": 1,
                "uservisible": true,
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":20,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 1,
                    "timecompleted": 1634583712,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 22,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=22",
                "name": "Sororidad",
                "instance": 3,
                "contextid": 142,
                "visible": 1,
                "uservisible": true,
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":21,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            }
        ]
    },
    {
        "id": 2,
        "name": "Módulo 1",
        "visible": 1,
        "summary": "",
        "summaryformat": 1,
        "section": 1,
        "hiddenbynumsections": 0,
        "uservisible": true,
        "modules": [
            {
                "id": 23,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=23",
                "name": "1.1. Quién es tu clientela",
                "instance": 4,
                "contextid": 143,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=22\">Sororidad</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":22,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 24,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=24",
                "name": "1.2. ¿Por qué la gente compra un producto?",
                "instance": 5,
                "contextid": 144,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=23\">1.1. Quién es tu clientela</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":23,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 25,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=25",
                "name": "1.3. ¡Haz que tu tienda esté siempre abastecida!",
                "instance": 6,
                "contextid": 145,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=24\">1.2. ¿Por qué la gente compra un producto?</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":24,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 26,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=26",
                "name": "1.4. Acomodo de la tienda",
                "instance": 7,
                "contextid": 146,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=25\">1.3. ¡Haz que tu tienda esté siempre abastecida!</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":25,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            }
        ]
    },
    {
        "id": 3,
        "name": "Módulo 2",
        "visible": 1,
        "summary": "",
        "summaryformat": 1,
        "section": 2,
        "hiddenbynumsections": 0,
        "uservisible": true,
        "modules": [
            {
                "id": 27,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=27",
                "name": "2.1. Herramientas Tecnológicas para la Tienda",
                "instance": 8,
                "contextid": 147,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=26\">1.4. Acomodo de la tienda</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":26,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 28,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=28",
                "name": "2.2. Pasos para iniciar con Whatsapp",
                "instance": 9,
                "contextid": 148,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=27\">2.1. Herramientas Tecnológicas para la Tienda</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":27,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 29,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=29",
                "name": "2.3. Pagos Electrónicos",
                "instance": 10,
                "contextid": 149,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=28\">2.2. Pasos para iniciar con Whatsapp</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":28,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            }
        ]
    },
    {
        "id": 4,
        "name": "Módulo 3",
        "visible": 1,
        "summary": "",
        "summaryformat": 1,
        "section": 3,
        "hiddenbynumsections": 0,
        "uservisible": true,
        "modules": [
            {
                "id": 30,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=30",
                "name": "3.1 Las ganancias reales de tu tienda",
                "instance": 11,
                "contextid": 150,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=29\">2.3. Pagos Electrónicos</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":29,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 31,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=31",
                "name": "3.2 ¿En qué Momentos no Ganas ni Pierdes?",
                "instance": 12,
                "contextid": 151,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=30\">3.1 Las ganancias reales de tu tienda</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":30,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 32,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=32",
                "name": "3.3 Más Vale Prevenir",
                "instance": 13,
                "contextid": 152,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=31\">3.2 ¿En qué Momentos no Ganas ni Pierdes?</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":31,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": true,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            },
            {
                "id": 33,
                "url": "https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=33",
                "name": "3.4 Ahorra y Compra Tranquilidad",
                "instance": 14,
                "contextid": 153,
                "visible": 1,
                "uservisible": true,
                "availabilityinfo": "No disponible, a menos que: La actividad <strong><a href=\"https://www.redtrastiendaanpec.com/mod/lesson/view.php?id=32\">3.3 Más Vale Prevenir</a></strong> está calificada como completada",
                "visibleoncoursepage": 1,
                "modicon": "https://www.redtrastiendaanpec.com/theme/image.php?theme=edumy&component=lesson&image=icon",
                "modname": "lesson",
                "modplural": "Lecciones",
                "availability": "{\"op\":\"&\",\"c\":[{\"type\":\"completion\",\"cm\":32,\"e\":1}],\"showc\":[true]}",
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "noviewlink": false,
                "completion": 1,
                "completiondata": {
                    "state": 0,
                    "timecompleted": 0,
                    "overrideby": null,
                    "valueused": false,
                    "hascompletion": true,
                    "isautomatic": false,
                    "istrackeduser": false,
                    "uservisible": true,
                    "details": []
                },
                "dates": []
            }
        ]
    }
];

export const dummyConversationsByUserId : IConversations = {
    "conversations": [
        {
            "id": 8,
            "name": "",
            "subname": null,
            "imageurl": null,
            "type": 1,
            "membercount": 2,
            "ismuted": false,
            "isfavourite": false,
            "isread": false,
            "unreadcount": 1,
            "members": [
                {
                    "id": 5,
                    "fullname": "Alumno Tres Ingenia",
                    "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=5",
                    "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f1?rev=1200",
                    "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f2?rev=1200",
                    "isonline": false,
                    "showonlinestatus": true,
                    "isblocked": false,
                    "iscontact": true,
                    "isdeleted": false,
                    "canmessageevenifblocked": false,
                    "canmessage": true,
                    "requirescontact": false,
                    "contactrequests": []
                }
            ],
            "messages": [
                {
                    "id": 29,
                    "useridfrom": 5,
                    "text": "<p>hola amigo, cómo te va?</p>",
                    "timecreated": 1632255086
                }
            ],
            "candeletemessagesforallusers": true
        },
        {
            "id": 7,
            "name": "Curso Estándar 3a parte",
            "subname": "estándar competencia atención cliente 3a parte",
            "imageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=g%2Fg1",
            "type": 2,
            "membercount": 3,
            "ismuted": false,
            "isfavourite": false,
            "isread": false,
            "unreadcount": 1,
            "members": [
                {
                    "id": 5,
                    "fullname": "Alumno Tres Ingenia",
                    "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=5",
                    "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f1?rev=1200",
                    "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f2?rev=1200",
                    "isonline": false,
                    "showonlinestatus": true,
                    "isblocked": false,
                    "iscontact": true,
                    "isdeleted": false,
                    "canmessageevenifblocked": false,
                    "canmessage": null,
                    "requirescontact": null,
                    "contactrequests": []
                }
            ],
            "messages": [
                {
                    "id": 28,
                    "useridfrom": 5,
                    "text": "<p>hola que tal, apenas comenzando el curso y todo bien!</p>",
                    "timecreated": 1632253658
                }
            ],
            "candeletemessagesforallusers": true
        },
        {
            "id": 3,
            "name": "",
            "subname": null,
            "imageurl": null,
            "type": 1,
            "membercount": 2,
            "ismuted": false,
            "isfavourite": false,
            "isread": true,
            "unreadcount": null,
            "members": [
                {
                    "id": 3,
                    "fullname": "Alumno Uno Ingenia",
                    "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=3",
                    "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
                    "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
                    "isonline": false,
                    "showonlinestatus": true,
                    "isblocked": false,
                    "iscontact": true,
                    "isdeleted": false,
                    "canmessageevenifblocked": false,
                    "canmessage": true,
                    "requirescontact": false,
                    "contactrequests": []
                }
            ],
            "messages": [
                {
                    "id": 24,
                    "useridfrom": 4,
                    "text": "<p>me da mucho gusto!!</p>",
                    "timecreated": 1632251617
                }
            ],
            "candeletemessagesforallusers": true
        },
        {
            "id": 4,
            "name": "",
            "subname": null,
            "imageurl": null,
            "type": 3,
            "membercount": 1,
            "ismuted": false,
            "isfavourite": true,
            "isread": true,
            "unreadcount": null,
            "members": [
                {
                    "id": 4,
                    "fullname": "Alumno Dos Ingenia",
                    "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=4",
                    "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
                    "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
                    "isonline": false,
                    "showonlinestatus": true,
                    "isblocked": false,
                    "iscontact": false,
                    "isdeleted": false,
                    "canmessageevenifblocked": null,
                    "canmessage": null,
                    "requirescontact": null,
                    "contactrequests": []
                }
            ],
            "messages": [
                {
                    "id": 21,
                    "useridfrom": 4,
                    "text": "<p>me da mucho gusto!!</p>",
                    "timecreated": 1631729819
                }
            ],
            "candeletemessagesforallusers": true
        },
        {
            "id": 6,
            "name": "",
            "subname": null,
            "imageurl": null,
            "type": 1,
            "membercount": 2,
            "ismuted": false,
            "isfavourite": false,
            "isread": true,
            "unreadcount": null,
            "members": [
                {
                    "id": 2,
                    "fullname": "Admin User",
                    "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=2",
                    "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
                    "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
                    "isonline": false,
                    "showonlinestatus": true,
                    "isblocked": false,
                    "iscontact": false,
                    "isdeleted": false,
                    "canmessageevenifblocked": true,
                    "canmessage": true,
                    "requirescontact": false,
                    "contactrequests": []
                }
            ],
            "messages": [
                {
                    "id": 13,
                    "useridfrom": 2,
                    "text": "<p>hola amigo, cómo has estado</p>",
                    "timecreated": 1631727578
                }
            ],
            "candeletemessagesforallusers": true
        }
    ]
};


export const dummyConversationByUserIdAndConversationID : IConversation = {
  "id": 3,
  "name": 'null',
  "subname": 'null',
  "imageurl": 'null',
  "type": 1,
  "membercount": 2,
  "ismuted": false,
  "isfavourite": false,
  "isread": true,
  "unreadcount": 0,
  "members": [
      {
          "id": 3,
          "fullname": "Alumno Uno Ingenia",
          "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=3",
          "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
          "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
          "isonline": false,
          "showonlinestatus": true,
          "isblocked": false,
          "iscontact": true,
          "isdeleted": false,
          "canmessageevenifblocked": false,
          "canmessage": true,
          "requirescontact": false,
          "contactrequests": []
      }
  ],
  "messages": [
      {
          "id": 24,
          "useridfrom": 4,
          "text": "<p>me da mucho gusto!!</p>",
          "timecreated": 1632251617
      },
      {
          "id": 23,
          "useridfrom": 4,
          "text": "<p>hola de nuevo</p>",
          "timecreated": 1632249767
      },
      {
          "id": 22,
          "useridfrom": 4,
          "text": "<p>me da mucho gusto!!</p>",
          "timecreated": 1631729909
      },
      {
          "id": 20,
          "useridfrom": 3,
          "text": "<p>me da gusto</p>",
          "timecreated": 1631729584
      },
      {
          "id": 19,
          "useridfrom": 3,
          "text": "<p>me da gusto</p>",
          "timecreated": 1631729534
      },
      {
          "id": 17,
          "useridfrom": 4,
          "text": "<p>bien gracias y tu?</p>",
          "timecreated": 1631729381
      },
      {
          "id": 16,
          "useridfrom": 3,
          "text": "<p>hola amigo, cómo has estado</p>",
          "timecreated": 1631728652
      },
      {
          "id": 15,
          "useridfrom": 3,
          "text": "<p>hola amigo, cómo has estado</p>",
          "timecreated": 1631728599
      },
      {
          "id": 14,
          "useridfrom": 3,
          "text": "<p>hola amigo, cómo has estado</p>",
          "timecreated": 1631728582
      },
      {
          "id": 10,
          "useridfrom": 4,
          "text": "<p>pos descansa ;P<br>\nLOL !</p>",
          "timecreated": 1631229176
      },
      {
          "id": 9,
          "useridfrom": 3,
          "text": "<p>jajajajaja</p>",
          "timecreated": 1631229142
      },
      {
          "id": 8,
          "useridfrom": 3,
          "text": "<p>también, trabajando, pero ya me cansé :P</p>",
          "timecreated": 1631229098
      },
      {
          "id": 7,
          "useridfrom": 4,
          "text": "<p>trabajando, y tú?</p>",
          "timecreated": 1631229030
      },
      {
          "id": 6,
          "useridfrom": 3,
          "text": "<p>qué haces?</p>",
          "timecreated": 1631229009
      },
      {
          "id": 5,
          "useridfrom": 4,
          "text": "<p>hola amigo</p>",
          "timecreated": 1631228961
      },
      {
          "id": 4,
          "useridfrom": 3,
          "text": "<p>hola</p>",
          "timecreated": 1631228793
      },
      {
          "id": 3,
          "useridfrom": 3,
          "text": "<p>alumno</p>",
          "timecreated": 1631228782
      }
  ],
  "candeletemessagesforallusers": true
};

export const dummyContacts: IContacts = {
    "contacts": [
        {
            "id": 5,
            "fullname": "Alumno Tres Ingenia",
            "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=5",
            "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f1?rev=1200",
            "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f2?rev=1200",
            "isonline": false,
            "showonlinestatus": true,
            "isblocked": false,
            "iscontact": true,
            "isdeleted": false,
            "canmessageevenifblocked": false,
            "canmessage": false,
            "requirescontact": false,
            "contactrequests": [],
            "conversations": []
        },
        {
            "id": 3,
            "fullname": "Alumno Uno Ingenia",
            "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=3",
            "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
            "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
            "isonline": false,
            "showonlinestatus": true,
            "isblocked": false,
            "iscontact": true,
            "isdeleted": false,
            "canmessageevenifblocked": false,
            "canmessage": false,
            "requirescontact": false,
            "contactrequests": [],
            "conversations": [
                {
                    "id": 3,
                    "type": 1,
                    "name": 'null',
                    "timecreated": 1631228782
                }
            ]
        }
    ],
    "noncontacts": [
        {
            "id": 2,
            "fullname": "Admin User",
            "profileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/profile.php?id=2",
            "profileimageurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1",
            "profileimageurlsmall": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff2",
            "isonline": false,
            "showonlinestatus": true,
            "isblocked": false,
            "iscontact": false,
            "isdeleted": false,
            "canmessageevenifblocked": false,
            "canmessage": false,
            "requirescontact": false,
            "contactrequests": [],
            "conversations": [
                {
                    "id": 6,
                    "type": 1,
                    "name": 'null',
                    "timecreated": 1631727505
                }
            ]
        }
    ]
};

export const dummyGetPostsMercadoSection: IMercadoPosts = {
    "posts": [
        {
            "id": 11,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida amigos",
            "messageformat": 1,
            "author": {
                "id": 3,
                "fullname": "Alumno Uno Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1633311739,
            "timemodified": 1633311739,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p11",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=11",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=11",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=11",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=11",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=11#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-10-03T20:42:19-05:00\">Sunday, 3 October 2021, 8:42 PM</time>"
            }
        },
        {
            "id": 10,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida amigos",
            "messageformat": 1,
            "author": {
                "id": 3,
                "fullname": "Alumno Uno Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1633054562,
            "timemodified": 1633054562,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p10",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=10",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=10",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=10",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=10",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=10#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-09-30T21:16:02-05:00\">Thursday, 30 September 2021, 9:16 PM</time>"
            }
        },
        {
            "id": 9,
            "subject": "Re: Bienvenidos",
            "replysubject": "Re: Bienvenidos",
            "message": "<p dir=\"ltr\" style=\"text-align:left;\"><img src=\"https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/124/mod_forum/post/9/Mask%20Group%2088.png\" alt=\"imagen demo prueba\" width=\"1200\" height=\"360\" class=\"img-fluid atto_image_button_text-bottom\" /><br /></p><p dir=\"ltr\" style=\"text-align:left;\"><br /></p><p dir=\"ltr\" style=\"text-align:left;\">Salludos a todos</p>",
            "messageformat": 1,
            "author": {
                "id": 5,
                "fullname": "Alumno Tres Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=5&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/75/user/icon/edumy/f1?rev=1200"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632959601,
            "timemodified": 1632959601,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p9",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=9",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=9",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=9",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=9",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=9#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [
                {
                    "contextid": 124,
                    "component": "mod_forum",
                    "filearea": "attachment",
                    "itemid": 9,
                    "filepath": "/",
                    "filename": "Lenguaje-de-programacion-JavaScript-1.pdf",
                    "isdir": false,
                    "isimage": false,
                    "timemodified": 1632959602,
                    "timecreated": 1632959593,
                    "filesize": 311017,
                    "author": "Alumno Tres Ingenia",
                    "license": "unknown",
                    "filenameshort": "Lenguaje-de-program...pdf",
                    "filesizeformatted": "303.7KB",
                    "icon": "f/pdf-128",
                    "timecreatedformatted": "Wednesday, 29 September 2021, 6:53 PM",
                    "timemodifiedformatted": "Wednesday, 29 September 2021, 6:53 PM",
                    "url": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/124/mod_forum/attachment/9/Lenguaje-de-programacion-JavaScript-1.pdf?forcedownload=1",
                    "urls": {
                        "export": null
                    },
                    "html": {
                        "plagiarism": null
                    }
                }
            ],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=5&course=10\">Alumno Tres Ingenia</a> - <time datetime=\"2021-09-29T18:53:21-05:00\">Wednesday, 29 September 2021, 6:53 PM</time>"
            }
        },
        {
            "id": 8,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida amigos",
            "messageformat": 1,
            "author": {
                "id": 3,
                "fullname": "Alumno Uno Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632861283,
            "timemodified": 1632861283,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p8",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=8",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=8",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=8",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=8",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=8#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-09-28T15:34:43-05:00\">Tuesday, 28 September 2021, 3:34 PM</time>"
            }
        },
        {
            "id": 7,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida amigos",
            "messageformat": 1,
            "author": {
                "id": 3,
                "fullname": "Alumno Uno Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632516066,
            "timemodified": 1632516066,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p7",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=7",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=7",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=7",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=7",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=7#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-09-24T15:41:06-05:00\">Friday, 24 September 2021, 3:41 PM</time>"
            }
        },
        {
            "id": 6,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida amigos",
            "messageformat": 1,
            "author": {
                "id": 3,
                "fullname": "Alumno Uno Ingenia",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632516009,
            "timemodified": 1632516009,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p6",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=6",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=6",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=6",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=6",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=6#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-09-24T15:40:09-05:00\">Friday, 24 September 2021, 3:40 PM</time>"
            }
        },
        {
            "id": 5,
            "subject": "bienvenida",
            "replysubject": "Re: bienvenida",
            "message": "gracias por la bienvenida",
            "messageformat": 1,
            "author": {
                "id": 2,
                "fullname": "Admin User",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632515933,
            "timemodified": 1632515933,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p5",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=5",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=5",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=5",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=5",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=5#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10\">Admin User</a> - <time datetime=\"2021-09-24T15:38:53-05:00\">Friday, 24 September 2021, 3:38 PM</time>"
            }
        },
        {
            "id": 4,
            "subject": "duda",
            "replysubject": "Re: duda",
            "message": "tengo una duda compañero",
            "messageformat": 1,
            "author": {
                "id": 2,
                "fullname": "Admin User",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632173235,
            "timemodified": 1632173235,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p4",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=4",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=4",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=4",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=4",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=4#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10\">Admin User</a> - <time datetime=\"2021-09-20T16:27:15-05:00\">Monday, 20 September 2021, 4:27 PM</time>"
            }
        },
        {
            "id": 3,
            "subject": "Re: Bienvenidos",
            "replysubject": "Re: Bienvenidos",
            "message": "<div class=\"text_to_html\">hola buenas tardes, ¿Qué se puede hacer en este espacio?</div>",
            "messageformat": 1,
            "author": {
                "id": 2,
                "fullname": "Admin User",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1"
                }
            },
            "discussionid": 2,
            "hasparent": true,
            "parentid": 2,
            "timecreated": 1632172379,
            "timemodified": 1632172379,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": true,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p3",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=3",
                "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=3",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=3",
                "split": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?prune=3",
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=3#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10\">Admin User</a> - <time datetime=\"2021-09-20T16:12:59-05:00\">Monday, 20 September 2021, 4:12 PM</time>"
            }
        },
        {
            "id": 2,
            "subject": "Bienvenidos",
            "replysubject": "Re: Bienvenidos",
            "message": "<p dir=\"ltr\" style=\"text-align:left;\"></p><div><div><p dir=\"ltr\">Red Trastienda es un espacio seguro para promocionar tus productos, servicios y conocimientos. <strong>Esta red de apoyo es para que JUNTOS salgamos adelante</strong>.</p></div></div><br /><p></p>",
            "messageformat": 1,
            "author": {
                "id": 2,
                "fullname": "Admin User",
                "isdeleted": false,
                "groups": [],
                "urls": {
                    "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10",
                    "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/theme/image.php?theme=edumy&component=core&image=u%2Ff1"
                }
            },
            "discussionid": 2,
            "hasparent": false,
            "parentid": null,
            "timecreated": 1632171087,
            "timemodified": 1632171087,
            "unread": null,
            "isdeleted": false,
            "isprivatereply": false,
            "haswordcount": false,
            "wordcount": null,
            "charcount": null,
            "capabilities": {
                "view": true,
                "edit": true,
                "delete": true,
                "split": false,
                "reply": true,
                "selfenrol": false,
                "export": false,
                "controlreadstatus": false,
                "canreplyprivately": true
            },
            "urls": {
                "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
                "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=2",
                "viewparent": null,
                "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=2",
                "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=2",
                "split": null,
                "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=2#mformforum",
                "export": null,
                "markasread": null,
                "markasunread": null,
                "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
            },
            "attachments": [],
            "tags": [],
            "html": {
                "rating": null,
                "taglist": null,
                "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=2&course=10\">Admin User</a> - <time datetime=\"2021-09-20T15:51:27-05:00\">Monday, 20 September 2021, 3:51 PM</time>"
            }
        }
    ],
    "forumid": 9,
    "courseid": 10,
    "ratinginfo": {
        "contextid": 124,
        "component": "mod_forum",
        "ratingarea": "post",
        "canviewall": null,
        "canviewany": null,
        "scales": [],
        "ratings": []
    },
    "warnings": []
};

export const dummyReplyPostMercadoSection: IReplyPost = {
    "postid": 12,
    "warnings": [],
    "post": {
        "id": 12,
        "subject": "bienvenida",
        "replysubject": "Re: bienvenida",
        "message": "gracias por la bienvenida amigos",
        "messageformat": 1,
        "author": {
            "id": 3,
            "fullname": "Alumno Uno Ingenia",
            "isdeleted": false,
            "groups": [],
            "urls": {
                "profile": "https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10",
                "profileimage": "https://repo.ingenia.com/ascoca_app/Code/moodle/pluginfile.php/30/user/icon/edumy/f1?rev=2668"
            }
        },
        "discussionid": 2,
        "hasparent": true,
        "parentid": 2,
        "timecreated": 1633314271,
        "timemodified": 1633314271,
        "unread": null,
        "isdeleted": false,
        "isprivatereply": false,
        "haswordcount": false,
        "wordcount": null,
        "charcount": null,
        "capabilities": {
            "view": true,
            "edit": true,
            "delete": true,
            "split": false,
            "reply": true,
            "selfenrol": false,
            "export": false,
            "controlreadstatus": false,
            "canreplyprivately": false
        },
        "urls": {
            "view": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p12",
            "viewisolated": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2&parent=12",
            "viewparent": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2#p2",
            "edit": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?edit=12",
            "delete": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?delete=12",
            "split": null,
            "reply": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/post.php?reply=12#mformforum",
            "export": null,
            "markasread": null,
            "markasunread": null,
            "discuss": "https://repo.ingenia.com/ascoca_app/Code/moodle/mod/forum/discuss.php?d=2"
        },
        "attachments": [],
        "tags": [],
        "html": {
            "rating": null,
            "taglist": null,
            "authorsubheading": "by <a href=\"https://repo.ingenia.com/ascoca_app/Code/moodle/user/view.php?id=3&course=10\">Alumno Uno Ingenia</a> - <time datetime=\"2021-10-03T21:24:31-05:00\">Sunday, 3 October 2021, 9:24 PM</time>"
        }
    },
    "messages": [
        {
            "type": "success",
            "message": "Your post was successfully added."
        },
        {
            "type": "success",
            "message": "You have 30 mins to edit it if you want to make any changes."
        }
    ]
};

export const dummyPostGetNewsMisLegisladores: IEntries = {
    "entries": [
        {
            "id": 19,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 12",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632867346,
            "created": 1632867346,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/19/Mask%20Group%2057.png",
                    "timemodified": 1632867346,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 25,
                    "taginstancecontextid": 5,
                    "itemid": 19,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 18,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 11",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632867302,
            "created": 1632867301,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/18/Mask%20Group%2057.png",
                    "timemodified": 1632867302,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 24,
                    "taginstancecontextid": 5,
                    "itemid": 18,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 17,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem  ipsum 10",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632867255,
            "created": 1632867255,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/17/Mask%20Group%2057.png",
                    "timemodified": 1632867255,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 23,
                    "taginstancecontextid": 5,
                    "itemid": 17,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 16,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 9",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632866520,
            "created": 1632866519,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/16/Mask%20Group%2057.png",
                    "timemodified": 1632866519,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 22,
                    "taginstancecontextid": 5,
                    "itemid": 16,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 15,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 8",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632866443,
            "created": 1632866443,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/15/Mask%20Group%2057.png",
                    "timemodified": 1632866443,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 21,
                    "taginstancecontextid": 5,
                    "itemid": 15,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 14,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 7",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632866355,
            "created": 1632866354,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/14/Mask%20Group%2057.png",
                    "timemodified": 1632866354,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 20,
                    "taginstancecontextid": 5,
                    "itemid": 14,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 13,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 6",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632865275,
            "created": 1632865275,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/13/Mask%20Group%2057.png",
                    "timemodified": 1632865275,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 19,
                    "taginstancecontextid": 5,
                    "itemid": 13,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 12,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem ipsum 5",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632865224,
            "created": 1632865223,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/12/Mask%20Group%2057.png",
                    "timemodified": 1632865224,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 18,
                    "taginstancecontextid": 5,
                    "itemid": 12,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 4,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem Ipsum 4",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><br /><img src=\"https://fakeimg.pl/928x1080/\" alt=\"Lorem Ipsum\" width=\"928\" height=\"1080\" class=\"img-fluid atto_image_button_text-bottom\" /><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632277612,
            "created": 1630124568,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/4/Mask%20Group%2057.png",
                    "timemodified": 1632277612,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 12,
                    "taginstancecontextid": 5,
                    "itemid": 4,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 3,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Lorem Ipsum 3",
            "summary": "<p dir=\"ltr\" style=\"text-align:left;\"></p><h2>What is Lorem Ipsum?</h2><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><img src=\"https://fakeimg.pl/928x1080/\" alt=\"Lorem Ipsum\" width=\"928\" height=\"1080\" class=\"img-fluid atto_image_button_text-bottom\" /><br /><p></p>",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632277704,
            "created": 1630124520,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Mask Group 57.png",
                    "filepath": "/",
                    "filesize": 232544,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/3/Mask%20Group%2057.png",
                    "timemodified": 1632277704,
                    "mimetype": "image/png",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 7,
                    "name": "mis legisladores",
                    "rawname": "Mis legisladores",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 13,
                    "taginstancecontextid": 5,
                    "itemid": 3,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        }
    ],
    "totalentries": 12,
    "warnings": []
};

export const dummypostGetNewsRevistaTrastienda: IEntries = {
    "entries": [
        {
            "id": 11,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Revista #4",
            "summary": "Lorem Ipsum Lorem ipsum",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632885720,
            "created": 1632428363,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Image-185.jpg",
                    "filepath": "/",
                    "filesize": 48846,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/11/Image-185.jpg",
                    "timemodified": 1632428363,
                    "mimetype": "image/jpeg",
                    "isexternalfile": false
                },
                {
                    "filename": "Lenguaje-de-programacion-JavaScript-1.pdf",
                    "filepath": "/",
                    "filesize": 311017,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/11/Lenguaje-de-programacion-JavaScript-1.pdf",
                    "timemodified": 1632428363,
                    "mimetype": "application/pdf",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 6,
                    "name": "revista trastienda",
                    "rawname": "Revista trastienda",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 17,
                    "taginstancecontextid": 5,
                    "itemid": 11,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 10,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Revista #3",
            "summary": "Lorem ipsum",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632885663,
            "created": 1632428164,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Image-185.jpg",
                    "filepath": "/",
                    "filesize": 48846,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/10/Image-185.jpg",
                    "timemodified": 1632428165,
                    "mimetype": "image/jpeg",
                    "isexternalfile": false
                },
                {
                    "filename": "Lenguaje-de-programacion-JavaScript-1.pdf",
                    "filepath": "/",
                    "filesize": 311017,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/10/Lenguaje-de-programacion-JavaScript-1.pdf",
                    "timemodified": 1632428165,
                    "mimetype": "application/pdf",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 6,
                    "name": "revista trastienda",
                    "rawname": "Revista trastienda",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 16,
                    "taginstancecontextid": 5,
                    "itemid": 10,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 9,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Revista #2",
            "summary": "Lorem Ipsum",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632885796,
            "created": 1632270555,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Image 185.png",
                    "filepath": "/",
                    "filesize": 216763,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/9/Image%20185.png",
                    "timemodified": 1632862719,
                    "mimetype": "image/png",
                    "isexternalfile": false
                },
                {
                    "filename": "Image-185.pdf",
                    "filepath": "/",
                    "filesize": 25651,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/9/Image-185.pdf",
                    "timemodified": 1632270555,
                    "mimetype": "application/pdf",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 6,
                    "name": "revista trastienda",
                    "rawname": "Revista trastienda",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 11,
                    "taginstancecontextid": 5,
                    "itemid": 9,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        },
        {
            "id": 8,
            "module": "blog",
            "userid": 2,
            "courseid": 0,
            "groupid": 0,
            "moduleid": 0,
            "coursemoduleid": 0,
            "subject": "Revista #1",
            "summary": "Lorem Ipsum",
            "summaryformat": 1,
            "content": null,
            "uniquehash": "",
            "rating": 0,
            "format": 1,
            "attachment": "1",
            "publishstate": "site",
            "lastmodified": 1632885904,
            "created": 1632270287,
            "usermodified": null,
            "summaryfiles": [],
            "attachmentfiles": [
                {
                    "filename": "Image-185.jpg",
                    "filepath": "/",
                    "filesize": 48846,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/8/Image-185.jpg",
                    "timemodified": 1632862652,
                    "mimetype": "image/jpeg",
                    "isexternalfile": false
                },
                {
                    "filename": "Lenguaje-de-programacion-JavaScript-1.pdf",
                    "filepath": "/",
                    "filesize": 311017,
                    "fileurl": "https://repo.ingenia.com/ascoca_app/Code/moodle/webservice/pluginfile.php/1/blog/attachment/8/Lenguaje-de-programacion-JavaScript-1.pdf",
                    "timemodified": 1632270287,
                    "mimetype": "application/pdf",
                    "isexternalfile": false
                }
            ],
            "tags": [
                {
                    "id": 6,
                    "name": "revista trastienda",
                    "rawname": "Revista trastienda",
                    "isstandard": false,
                    "tagcollid": 1,
                    "taginstanceid": 10,
                    "taginstancecontextid": 5,
                    "itemid": 8,
                    "ordering": 0,
                    "flag": 0
                }
            ]
        }
    ],
    "totalentries": 4,
    "warnings": []
};